# About
C# .NET<br>

Pull Me Over is a script for GTA V, and was created in order to have more realistic driving experience in GTA by having enforced traffic laws. The script tracks player's every move and will trigger nearest AI police if player has violated any traffic rules. The violations include speeding, running a red light, running a stop sign, etc..

The script is published at [https://www.gta5-mods.com/scripts/pull-me-over-0-8](https://www.gta5-mods.com/scripts/pull-me-over-0-8) 

Cleaner version is at [https://github.com/jukkaove/pullmeover](https://github.com/jukkaove/pullmeover)
