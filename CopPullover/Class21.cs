﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;

using System.Windows.Input;
using System.Windows.Forms;

public class CopPullover : Script
{
    enum CopStates { None, Chase, Follow, Ticket, Pursuit, Pulled };
    static CopStates state;
    bool s;
    float playerspeed;
    float finalplayerspeed;
    bool scriptenabled;
    bool keyupped;
    bool abort;

    int pursuitTicks;
    int closeticks = 0;
    int ticks;
    Vehicle[] vehs;
    Random rand;
    int keycount = 0;
    string[] sv = { "s_m_y_sheriff_01_white_full_01", "s_m_y_sheriff_01_white_full_02" };
    string[] cv = { "s_m_y_cop_01_white_full_01", "s_m_y_cop_01_white_full_02", "s_m_y_cop_01_black_full_01", "s_m_y_cop_01_black_full_02" };
    string[] fv = { "s_f_y_cop_01_white_full_01", "s_f_y_cop_01_white_full_02", "s_f_y_cop_01_black_full_01", "s_f_y_cop_01_black_full_02" };
    string[] freeways = { "Los Santos Freeway", "Del Perro Freeway", "Great Ocean Highway", "La Puerta Freeway", "Elysian Fields Freeway", "Palomino Freeway", "Senora Freeway",
                          "Los Santos Fwy", "Del Perro Fwy", "Great Ocean Highway", "La Puerta Fwy", "Elysian Fields Fwy", "Palomino Fwy", "Senora Fwy", "Olympic Fwy"};

    float speedlimit;
    string currentstreet;
    string laststreet = "";
    bool canjoin;

    Ped copped;
    Vehicle copveh;

    public CopPullover()
    {
        scriptenabled = true;
        Tick += OnTick;
        KeyDown += OnKeyDown;
        KeyUp += OnKeyUp;
        pursuitTicks = 0;
        Interval = 500;
        ticks = 0;
        //vehs = null;
        rand = new Random();
        vehs = World.GetNearbyVehicles(Game.Player.Character.Position, 1);
        state = CopStates.None;
    }

    void OnTick(object sender, EventArgs e)
    {
        
        if (Game.Player.Character.IsInVehicle() && scriptenabled)
        {
            playerspeed = Game.Player.Character.CurrentVehicle.Speed;
            UI.ShowSubtitle(playerspeed + " limit " + speedlimit + " state " + state);
            currentstreet = World.GetStreetName(Game.Player.Character.Position);
            if (!currentstreet.Equals(laststreet))
            {
                laststreet = currentstreet;
                if (!freeways.Contains(currentstreet))
                    speedlimit = 60 / 2.236936f;
                else
                    speedlimit = 75 / 2.236936f;
            }
        }
        if (Game.Player.Character.IsInVehicle() && scriptenabled && state == CopStates.None)
        {
            if (playerspeed > speedlimit && Game.Player.WantedLevel == 0)
            {
                UI.ShowSubtitle("speeding", 100);
                Vehicle copv = GetCopVeh();
                state = CopStates.Pursuit;
                if(copv != null)
                {
                    copveh = copv;
                    if (!copveh.IsSeatFree(VehicleSeat.Driver))
                    {
                        Ped copp = GetCopPed(copv);
                        finalplayerspeed = Game.Player.Character.CurrentVehicle.Speed;
                        if (copp != null)
                            FollowPlayer();
                    } else
                    {
                        copveh = null;
                        copped = null;
                        state = CopStates.None;
                    }
                }
                else
                    state = CopStates.None;
            }

        }
       
        //   if (!copped.Exists())
        //       copped = null;
        //  if (!copveh.Exists())
        //      copveh = null;
    }

    Ped GetCopPed(Vehicle vehc)
    {
        copped = copveh.GetPedOnSeat(VehicleSeat.Driver);
        UI.ShowSubtitle("getped", 100);
        if (!Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, copped.Handle, Game.Player.Character.Handle, 17))
            return null;
          
        if (vehc.Model == GTA.Native.VehicleHash.Policeb)
            copped.Weapons.Remove(WeaponHash.Pistol);

            copped = copveh.GetPedOnSeat(VehicleSeat.Driver);
            copped.IsPersistent = true;
            vehc.IsPersistent = true;
//        Function.Call(Hash.REMOVE_PED_FROM_GROUP, copped.Handle);
//        copped.RelationshipGroup = World.AddRelationshipGroup("cop");
//        World.SetRelationshipBetweenGroups(Relationship.Pedestrians, Game.Player.Character.RelationshipGroup, copped.RelationshipGroup);
//        World.SetRelationshipBetweenGroups(Relationship.Pedestrians, copped.RelationshipGroup, Game.Player.Character.RelationshipGroup);

        return copveh.GetPedOnSeat(VehicleSeat.Driver);
        
    }

    Vehicle GetCopVeh()
    {
        UI.ShowSubtitle("getveh", 100);
        vehs = World.GetNearbyVehicles(Game.Player.Character.Position, 50);
        for (int i = 0; i < vehs.Length; i++)
        {
            if (vehs[i].Model == GTA.Native.VehicleHash.Police ||
                vehs[i].Model == GTA.Native.VehicleHash.Police2 ||
                vehs[i].Model == GTA.Native.VehicleHash.Police3 ||
                vehs[i].Model == GTA.Native.VehicleHash.Sheriff ||
                vehs[i].Model == GTA.Native.VehicleHash.Sheriff2 ||
                vehs[i].Model == GTA.Native.VehicleHash.Policeb)
            {
                Vehicle temp = Game.Player.Character.CurrentVehicle;
                if (!vehs[i].SirenActive && vehs[i] != temp)
                {
                    return vehs[i];
                }
            }
        }
        return null;
    }

    float headu()
    {
        float playh = Game.Player.Character.Heading;
        float coph = copped.Heading;

        if (coph > playh)
            return coph - playh;
        else
            return playh - coph;
    }

    void FollowPlayer()
    {
        state = CopStates.Pursuit;
        UI.Notify("Follow");       
        UI.Notify("line129");
        UI.ShowSubtitle("line130", 100);

         while (!copped.IsDead || Game.Player.Character.IsInVehicle())
         {
             Wait(100);
             UI.Notify("line132");
             UI.ShowSubtitle("line134", 100);
             Wait(100);
             if (!Function.Call<bool>(Hash.IS_PED_FACING_PED, Game.Player.Character, copped, 145f))
             {
                 UI.ShowSubtitle("line137", 100);
                 break;
             }

             UI.ShowSubtitle("line141", 100);
             UI.Notify("line142");
             Wait(0);
         }
        UI.ShowSubtitle("line145", 100);
        if (copped.IsDead || !Game.Player.Character.IsInVehicle())
        {
            UI.ShowSubtitle("line148", 100);
            UI.Notify("line141");
            EndEvent();
            return;
        }
        UI.ShowSubtitle("line153", 100);
        Interval = 100; UI.Notify("line146");
        copped.Task.ClearAll();
        Function.Call(Hash.TASK_VEHICLE_CHASE, copped, Game.Player.Character.CurrentVehicle.Handle);
        UI.Notify("line148");
        UI.Notify("chase");
        bool ticket = false;
        bool lights = false;
        bool warned = false;
        float dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
        Vehicle playv = Game.Player.Character.CurrentVehicle;
        float tspeed = playv.Speed;
        float copheading;
        float playerheading;
        float headingdiff;
        closeticks = 0;

        while (Game.Player.WantedLevel == 0 && copped.IsAlive)
        {
            UI.Notify("line160");
            copheading = copped.Heading;
            playerheading = Game.Player.Character.Heading;
            headingdiff = Math.Abs(copheading - playerheading);
            dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
            tspeed = playv.Speed;
            playerspeed = Game.Player.Character.CurrentVehicle.Speed;

            if (playerspeed > finalplayerspeed)
                finalplayerspeed = playerspeed;

            if(!warned)
            {
                UI.Notify("line173");
                if (dist > 30 && headingdiff > 25)
                    continue;
                else
                {
                    Function.Call(Hash.DISABLE_VEHICLE_IMPACT_EXPLOSION_ACTIVATION, copveh, true);
                    copveh.SirenActive = true;
                    warned = true;
                }
            }
            else
            {
                if (dist < 50)
                {
                    closeticks++;
                    if (dist < 30)
                    {
                        if (closeticks % 600 == 0)
                        {
                            Function.Call(Hash.BLIP_SIREN, copveh);
                            Wait(500);
                            Function.Call(Hash._0x3523634255FC3318, Game.Player.Character, "STOP_VEHICLE_GENERIC_MEGAPHONE", getCopVoice(copped), "SPEECH_PARAMS_FORCE", 0);
                        }
                        if (dist >= 20 && state != CopStates.Chase)
                        {
                            state = CopStates.Chase;
                            Function.Call(Hash.TASK_VEHICLE_CHASE, copped, Game.Player.Character.Handle);
                        }
                        else if (dist < 20 && dist >= 12 && state != CopStates.Follow)
                        {
                            state = CopStates.Follow;
                            Function.Call(Hash._TASK_VEHICLE_FOLLOW, copped, copveh, playv.Handle, 6, 50, 10);
                        }
                        else if (dist < 12 && tspeed == 0 && copveh.Speed < 2)
                        {
                            ticket = true;
                            break;
                        }
                        if (copveh.HasCollidedWithAnything)
                        {
                            if (Game.Player.WantedLevel == 1)
                                Game.Player.WantedLevel = 0;
                        }
                    }
                    if (closeticks > 3000)
                    {
                        Game.Player.WantedLevel = 1;
                        break;
                    }
                    UI.Notify("line222");
                }
                else if (dist > 350)
                    break;
            }
            
            Wait(0);
        }

        if (ticket)
            TicketTime();
        else
        {
            EndEvent();
            copped.Task.ClearAll();
        }
    }

    void TicketTime()
    {
        UI.Notify("Ticket");
        state = CopStates.Pulled;
        Wait(2000);
        copped.Task.LeaveVehicle(copveh, true);
        Vector3 finalstop = Game.Player.Character.Position;
    //    Wait(2000);
        copped.Task.ClearAll();
        if (copped.IsDead)
        { EndEvent(); return; }

        Vector3 pos = Vector3.Cross(Game.Player.Character.ForwardVector, -Game.Player.Character.UpVector);
        float dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
        float dist2 = Vector3.Distance(Game.Player.Character.Position, finalstop);
        
        copped.Task.GoTo(Game.Player.Character.Position + pos, false, -1);
       // copped.AlwaysKeepTask = true;
        ticks = 0;
        UI.Notify("out");
        while (true)
        {
            dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
            dist2 = Vector3.Distance(Game.Player.Character.Position, finalstop);
            pos = Vector3.Cross(Game.Player.Character.ForwardVector, -Game.Player.Character.UpVector);
            ticks++;
          //  copped.Task.GoTo(Game.Player.Character.Position + pos, false, -1);
            /* if (dist2 > 5 || !Game.Player.Character.IsInVehicle())
             {
                 copped.Task.ClearAll();
                 Game.Player.WantedLevel = 1;
                 EndEvent();
                 break;
             }*/

            if (dist2 < 2f)
            {
                if (dist < 2.2f)
                    break;
                else
                    copped.Task.GoTo(Game.Player.Character.Position + pos, false, -1);
            }

            else if (ticks > 800)
                break;
            UI.ShowSubtitle(ticks + " " + dist2, 500);
            Wait(0);
        }
        copped.AlwaysKeepTask = false;
        UI.Notify("broke");
        if (copped.IsDead || Game.Player.WantedLevel > 0) { EndEvent(); return; }
        if (ticks > 800)
        {
            EndEvent();

            UI.Notify("~r~Pullover aborted"); 
            return;
        }
        copped.Task.ClearAll();
        Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_HI", "SPEECH_PARAMS_FORCE");
        copped.Task.TurnTo(Game.Player.Character, 1500);
        Wait(500);
        Function.Call(Hash._PLAY_AMBIENT_SPEECH2, Game.Player.Character, "GENERIC_HI", "SPEECH_PARAMS_FORCE");

        Wait(1000);
        Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "KIFFLOM_GREET", "SPEECH_PARAMS_FORCE");
        Function.Call(Hash.TASK_START_SCENARIO_IN_PLACE, copped, "CODE_HUMAN_MEDIC_TIME_OF_DEATH", 0, 1);
        Wait(5000);
        Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_CURSE_HIGH", "SPEECH_PARAMS_FORCE");
        UI.ShowSubtitle("Cop: I clocked you going ~r~" + (int)(finalplayerspeed * 2.236936f) + " ~w~in a " + (int)(speedlimit * 2.236936f) + " MPH zone!", 5000);
        ticks = 0;

        while (!copped.IsDead)
        {
            dist2 = Vector3.Distance(Game.Player.Character.Position, finalstop);
            if (dist2 > 5 || !Game.Player.Character.IsInVehicle())
            {
                copped.Task.ClearAll();
                Game.Player.WantedLevel = 1;
                EndEvent();
                break;
            }

            if (!Function.Call<bool>(Hash.IS_PED_USING_SCENARIO, "CODE_HUMAN_MEDIC_TIME_OF_DEATH", copped))
                break;
            ticks++;

            if (ticks > 3000)
            {
                copped.Task.ClearAll();
                break;
            }
                
            UI.ShowSubtitle(ticks + "", 500);
            Wait(0);
        }

        if (copped.IsDead || Game.Player.WantedLevel > 0) { EndEvent(); return; }

        int limit = (int)(speedlimit * 2.236936f);
        int pspeed = (int)(finalplayerspeed * 2.236936f);
        Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_BYE", "SPEECH_PARAMS_FORCE");
        UI.Notify("You were fined for going " + (pspeed - limit) + " over the speed limit. ~n~Fines: ~r~$" + (pspeed - limit) * 500);
        copped.Task.ClearAll();
        copped.Task.EnterVehicle(copveh, VehicleSeat.Driver);
        copped.IsPersistent = false;
        copveh.IsPersistent = false;
        
        state = CopStates.None;
        Interval = 500;

    }

    void EndEvent()
    {
        UI.Notify("EndEvent");
        copped.IsPersistent = false;
        copveh.IsPersistent = false;
        state = CopStates.None;
        Interval = 500;
    }

    string getCopVoice(Ped copped)
    {
        if (copped.Gender == Gender.Male)
        {
            if (copped.Model == PedHash.Sheriff01SMY)
            {
                return sv[rand.Next(sv.Length)];
            }
            else
            {
                return cv[rand.Next(cv.Length)];
            }
        }
        else
        {
            return fv[rand.Next(fv.Length)];
        }
    }

    void OnKeyDown(object sender, KeyEventArgs e)
    {

    }
    void OnKeyUp(object sender, KeyEventArgs e)
    {

    }
    
}


