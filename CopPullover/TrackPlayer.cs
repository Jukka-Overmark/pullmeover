﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;
using System.Windows.Forms;


public class TrackPlayer : Script
{
    // private ModTesting mod;
    Vehicle playerVehicle;
    PlayerInfo pinfo;
    CopPullover2 cp2;
    string[] freeways = { "Los Santos Freeway", "Del Perro Freeway", "Great Ocean Highway", "La Puerta Freeway", "Elysian Fields Freeway", "Palomino Freeway", "Senora Freeway",
                          "Los Santos Fwy", "Del Perro Fwy", "Great Ocean Highway", "La Puerta Fwy", "Elysian Fields Fwy", "Palomino Fwy", "Senora Fwy", "Olympic Fwy"};

    float speedlimit;
    string currentstreet;
    bool scriptenabled = true;
    bool keyupped;
    int keycount = 0;

    bool s;
    float playerspeed;

    int ticks;

    string rikepaikka;
    int againsttrafficticks = 0;
    
    string reason;
    int range = 75;

    static List<string> offences;
    int speed;

    public TrackPlayer()
    {
        Tick += OnTick;
        offences = new List<string>();
       // KeyDown += OnKeyDown;

    }

    void OnTick(object sender, EventArgs e)
    {
        if (Game.Player.Character.IsInVehicle())
        {
            
            if (!Game.Player.Character.CurrentVehicle.Equals(playerVehicle))
            {
                playerVehicle = Game.Player.Character.CurrentVehicle;
            }
                    


        }

        if (!World.GetStreetName(Game.Player.Character.Position).Equals(currentstreet))
        {
            currentstreet = World.GetStreetName(Game.Player.Character.Position);
            if (!freeways.Contains(currentstreet))
                speedlimit = 45 / 2.236936f;
            else
                speedlimit = 75 / 2.236936f;
        }

        //  UI.ShowSubtitle("All good " + Game.Player.WantedLevel + " playerspeed " + playerspeed + " speedlimit " + speedlimit + " state " + state, 500);
        if (Game.Player.Character.IsInVehicle() && scriptenabled && Game.Player.WantedLevel == 0)
        {
            UI.ShowSubtitle("ticks");
            if (CopPullover2.state == CopPullover2.CopStates.Ticket)
                return;

            if (DrivingAgainstTraffic(Function.Call<int>(Hash.GET_TIME_SINCE_PLAYER_DROVE_AGAINST_TRAFFIC, Game.Player)))
            {
                UI.ShowSubtitle("againstt", 500);
                rikepaikka = World.GetStreetName(Game.Player.Character.Position);
                reason = "againsttraffic";
                if (CopPullover2.state == CopPullover2.CopStates.Chase || CopPullover2.state == CopPullover2.CopStates.Follow)
                {
                    AddOffence("againsttraffic");
                    return;
                }
                CopPullover2.GetCops(range, reason);
            }

            if (NotRoadworthy())
            {
                reason = "notrw";
                if (CopPullover2.state == CopPullover2.CopStates.Chase || CopPullover2.state == CopPullover2.CopStates.Follow)
                {
                    AddOffence("notrw");
                    return;
                }
                CopPullover2.GetCops(range, reason);
            }

            if (Game.Player.Character.CurrentVehicle.HasCollidedWithAnything && (Game.Player.Character.CurrentVehicle.Speed > 15))
            {
                UI.ShowSubtitle("törmas");
                reason = "collision";
                if (CopPullover2.state == CopPullover2.CopStates.Chase || CopPullover2.state == CopPullover2.CopStates.Follow)
                {
                    AddOffence("collision");
                    return;
                }
                CopPullover2.GetCops(range, reason);
            }
            if (Function.Call<bool>(Hash.IS_PED_RUNNING_MOBILE_PHONE_TASK, Game.Player.Character))
            {
                reason = "phone";
                if (CopPullover2.state == CopPullover2.CopStates.Chase || CopPullover2.state == CopPullover2.CopStates.Follow)
                {
                    AddOffence("phone");
                    return;
                }
                CopPullover2.GetCops(20, reason);
            }
            if (Tailgating())
            {
                // reason = "tailgating";
                // GetCops(range);
            }


            /* if(DrivingOnPavement())
              {
                  UI.ShowSubtitle("onpavement", 500);
                //  rikepaikka = World.GetStreetName(Game.Player.Character.Position);
                //  reason = "onpavement";
                //  GetCops();
              }*/


            //   Function.Call(Hash.DRAW_LINE, ppos.X, ppos.Y, ppos.Z, ppos.X + pos.X, ppos.Y + pos.Y, ppos.Z + pos.Z, 255, 0, 0, 255);

            playerspeed = Game.Player.Character.CurrentVehicle.Speed;

            if (playerspeed > (speedlimit + 7)/* || state == CopStates.Pursuit*/)
            {
                rikepaikka = World.GetStreetName(Game.Player.Character.Position);
                reason = "speeding";
                if (CopPullover2.state == CopPullover2.CopStates.Chase || CopPullover2.state == CopPullover2.CopStates.Follow)
                {
                    AddOffence("speeding");
                    return;
                }
                CopPullover2.GetCops(range, reason);
            }
        }
        //        Vector3 pos = Vector3.Cross(Game.Player.Character.ForwardVector, -Game.Player.Character.UpVector)*1.5f;
        //        Vector3 ppos = Game.Player.Character.Position;

        //        Function.Call(Hash.DRAW_LINE, ppos.X, ppos.Y, ppos.Z, ppos.X + pos.X, ppos.Y + pos.Y, ppos.Z + pos.Z, 255, 0, 0, 255);
        //  UI.ShowSubtitle(ticks + "", 100);
        ticks++;
        if (ticks > 1000)
            ticks = 0;
    }

   /* void OnKeyDown(object sender, KeyEventArgs e)
    {
        if (e.KeyCode == Keys.F11 && !keyupped)
        {
            keycount++;
            if (keycount >= 2)
            {
                keyupped = true;
                scriptenabled = !scriptenabled;
                UI.Notify("Pullover Script = " + scriptenabled);
                keycount = 0;
            }
        }

    }

    void OnKeyUp(object sender, KeyEventArgs e)
    {
        if (e.KeyCode == Keys.F11)
        {
            keyupped = false;
            keycount = 0;
        }
    }*/

    bool DrivingAgainstTraffic(int dat)
    {
        if (dat == 0)
            againsttrafficticks++;
        else
            againsttrafficticks = 0;

        if (againsttrafficticks > 40)
        {
            rikepaikka = World.GetStreetName(Game.Player.Character.Position);
            return true;
        }

        return false;

    }

    bool NotRoadworthy()
    {
        if (Game.Player.Character.CurrentVehicle.LeftHeadLightBroken && Game.Player.Character.CurrentVehicle.RightHeadLightBroken)
            return true;

        return false;
    }

    bool Tailgating()
    {
        if (Game.Player.Character.CurrentVehicle.Speed < 20)
            return false;

        Vector3 pos = Game.Player.Character.CurrentVehicle.ForwardVector * 6 + Game.Player.Character.Position;
        Vehicle[] vehs = World.GetNearbyVehicles(pos, 1);

        for (int i = 0; i < vehs.Length; i++)
        {
            if (!vehs[i].Equals(Game.Player.Character.CurrentVehicle))
                return true;
        }
        return false;
    }

    bool DrivingOnPavement()
    {
        if (Game.Player.Character.CurrentVehicle.Speed > 15 / 2.236936f)
        {
            rikepaikka = World.GetStreetName(Game.Player.Character.Position);
            return true;
        }

        return false;
    }

    public void Clearplayer()
    {
        offences.Clear();
    }

    public void AddOffence(string offence)
    {
        if (offences.Contains(offence))
            return;
        offences.Add(offence);
    }

    public static List<string> ReturnOffences()
    {
        return offences;
    }
}

class PlayerInfo
{
    

    
}

