﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;
using NativeUI;

namespace PullMeOveri
{
    public partial class PullMeOverMain1 : Script
    {
        UIMenu mymenu;
        UIMenu submenu;
        MenuPool mymenupool;

        List<dynamic> speeds = new List<dynamic> { "20", "25", "30", "35", "40", "45", "50", "55", "60",
        "65", "70", "75", "80", "85", "90", "95", "100", "105", "110", "115", "120", "125", "130", "135", "140", "145", "150"};

        UIMenu speedmenu;
        UIMenuListItem paleto;
        UIMenuListItem grapeseed;
        UIMenuListItem sandyshores;
        UIMenuListItem delperro;
        UIMenuListItem davis;
        UIMenuListItem rockford;
        UIMenuListItem lossantos;
        UIMenuListItem general;
        UIMenuListItem highway;

        void SetUI()
        {
            UI.Notify("ui set");
            mymenupool = new MenuPool();
            mymenu = new UIMenu("PullMeOver", "Set things");
            mymenupool.Add(mymenu);
            speedmenu = new UIMenu("Speed Limits", "spiidit");
           
            submenu = mymenupool.AddSubMenu(mymenu, "Speed Limits");
            
            mymenu.AddItem(new UIMenuCheckboxItem("Grounded", false));
            paleto = new UIMenuListItem("Paleto", speeds, GetAreaSpeed("paleto"));
            grapeseed = new UIMenuListItem("Grapeseed", speeds, GetAreaSpeed("grapeseed"));
            sandyshores = new UIMenuListItem("Sandyshores", speeds, GetAreaSpeed("sandyshores"));
            delperro = new UIMenuListItem("Del Perro", speeds, GetAreaSpeed("delperro"));
            davis = new UIMenuListItem("Davis", speeds, GetAreaSpeed("davis"));
            rockford = new UIMenuListItem("Rockford", speeds, GetAreaSpeed("rockford"));
            lossantos = new UIMenuListItem("Los Santos", speeds, GetAreaSpeed("lossantos"));
            highway = new UIMenuListItem("Highway", speeds, GetAreaSpeed("highway"));
            general = new UIMenuListItem("General", speeds, GetAreaSpeed("general"));
            submenu.AddItem(paleto);
            submenu.AddItem(grapeseed);
            submenu.AddItem(sandyshores);
            submenu.AddItem(delperro);
            submenu.AddItem(davis);
            submenu.AddItem(rockford);
            submenu.AddItem(lossantos);
            submenu.AddItem(highway);
            submenu.AddItem(general);
            mymenu.RefreshIndex();
        }

        int GetAreaSpeed(string area)
        {
            switch (area)
            {
                case "paleto":
                    if(speeds.Contains(paletospeedlimit.ToString()))
                        return speeds.IndexOf(paletospeedlimit.ToString());
                    return 6;
                case "grapeseed":
                    if (speeds.Contains(grapeseedspeedlimit.ToString()))
                        return speeds.IndexOf(grapeseedspeedlimit.ToString());
                    return 6;
                case "sandyshores":
                    if (speeds.Contains(sandyshoresspeedlimit.ToString()))
                        return speeds.IndexOf(sandyshoresspeedlimit.ToString());
                    return 6;
                case "delperro":
                    if (speeds.Contains(delperrospeedlimit.ToString()))
                        return speeds.IndexOf(delperrospeedlimit.ToString());
                    return 6;
                case "davis":
                    if (speeds.Contains(davisspeedlimit.ToString()))
                        return speeds.IndexOf(davisspeedlimit.ToString());
                    return 6;
                case "rockford":
                    if (speeds.Contains(rockfordspeedlimit.ToString()))
                        return speeds.IndexOf(rockfordspeedlimit.ToString());
                    return 6;
                case "lossantos":
                    if (speeds.Contains(lossantosspeedlimit.ToString()))
                        return speeds.IndexOf(lossantosspeedlimit.ToString());
                    return 6;
                case "highway":
                    if (speeds.Contains(hwaylimit.ToString()))
                        return speeds.IndexOf(hwaylimit.ToString());
                    return 6;
                case "general":
                    if (speeds.Contains(generallimit.ToString()))
                        return speeds.IndexOf(generallimit.ToString());
                    return 6;
                default:
                    return 6;
            }
        }
    }
}
