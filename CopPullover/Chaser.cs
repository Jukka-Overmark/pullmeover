﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;

namespace PullMeOver
{
    class Chaser : PullMeOverMain
    {

        public Chaser(Vehicle copveh, Ped copped)
        {
        }

        void ExcessSpeeding(int rangex)
        {
            if (playerspeed - speedlimit < wlimit)
                return;

            if (Game.Player.WantedLevel == 0)
            {
                Ped[] cops = World.GetNearbyPeds(Game.Player.Character.Position, rangex);

                for (int i = 0; i < cops.Length; i++)
                {
                    if (Function.Call<int>(Hash.GET_PED_TYPE, cops[i]) == 6 || addonpeds.Contains(cops[i].Model.Hash))
                    {
                        if (!cops[i].Equals(Game.Player.Character)/* && cops[i].IsInVehicle()*/)
                        {
                            if (!Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, copped.Handle, Game.Player.Character.Handle, 17))
                                return;
                            Game.Player.WantedLevel = 1;
                        }
                    }
                }
            }
        }

        public void Action()
        {

            while (!IsInterrupted())
            {
                if (!Function.Call<bool>(Hash.IS_PED_FACING_PED, Game.Player.Character, copped, 120f))
                    break;

                Script.Wait(0);
            }

            if (IsInterrupted())
                return;

            int trailr = 0;
            if (Function.Call<bool>(Hash.IS_VEHICLE_ATTACHED_TO_TRAILER, playerVehicle))
            {
                if (playerVehicle.Model.Equals(VehicleHash.Bison) ||
                    playerVehicle.Model.Equals(VehicleHash.Bison2) ||
                    playerVehicle.Model.Equals(VehicleHash.Bison3) ||
                    playerVehicle.Model.Equals(VehicleHash.Sadler) ||
                    playerVehicle.Model.Equals(VehicleHash.Sadler2))
                {
                    trailr = 0;
                }
                else
                    trailr = 10;
            }

            Vehicle playv = playerVehicle;
            float tspeed = playv.Speed;
            //      Function.Call(Hash._TASK_VEHICLE_FOLLOW, copped, copveh, playv.Handle, 6, 50, 20);
            Function.Call(Hash.TASK_VEHICLE_CHASE, copped, Game.Player.Character.Handle);
            Function.Call(Hash.SET_TASK_VEHICLE_CHASE_IDEAL_PURSUIT_DISTANCE, copped, 50f);
            Function.Call(Hash.SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG, copped, 0, true);
            Function.Call(Hash.SET_DRIVER_ABILITY, copped, 0.9f);

            if (copped.Weapons.HasWeapon(WeaponHash.Pistol))
                copped.Weapons.Remove(WeaponHash.Pistol);
            if (copped.Weapons.HasWeapon(WeaponHash.StunGun))
                copped.Weapons.Remove(WeaponHash.StunGun);

            Function.Call(Hash.DISABLE_VEHICLE_IMPACT_EXPLOSION_ACTIVATION, copveh, true);
            copveh.SirenActive = true;

            bool ticket = false;
            bool lights = false;

            float dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
            int ticks = 0;

            while (!IsInterrupted())
            {

                dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
                tspeed = playv.Speed;
                playerspeed = playerVehicle.Speed;

                ExcessSpeeding(200);

                if (playerspeed > finalplayerspeed)
                {
                    rikepaikka = World.GetStreetName(Game.Player.Character.Position);
                    finalplayerspeed = playerspeed;
                }

                if (dist < 100)
                {
                    ticks++;
                    if (playv.Speed == 0)
                        ticks--;
                    UI.ShowSubtitle(dist + " ticks " + ticks);
                    if (dist + trailr < 35 + trailr && !lights && Function.Call<bool>(Hash.IS_PED_FACING_PED, copped, Game.Player.Character, 90f))
                    {
                        // Function.Call(Hash.DISABLE_VEHICLE_IMPACT_EXPLOSION_ACTIVATION, copveh, true);
                        // copveh.SirenActive = true;
                        Function.Call(Hash.BLIP_SIREN, copveh);
                        lights = true;
                    }
                    if (copveh.Speed < playerVehicle.Speed && state != CopStates.Chase)
                    {
                        state = CopStates.Chase;
                        Function.Call(Hash.TASK_VEHICLE_CHASE, copped, Game.Player.Character.Handle);
                        Function.Call(Hash.SET_TASK_VEHICLE_CHASE_IDEAL_PURSUIT_DISTANCE, copped, 50f);
                        Function.Call(Hash.SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG, copped, 0, true);
                    }
                    else if (Vector3.Distance(Game.Player.Character.Position, copped.Position) < 20 && state != CopStates.Follow)
                    {
                        state = CopStates.Follow;
                        Function.Call(Hash._TASK_VEHICLE_FOLLOW, copped, copveh, playv.Handle, 100, 0, Vector3.Distance(Game.Player.Character.Position, copped.Position) - 10);
                    }
                    if (state == CopStates.Follow && playerspeed < 1 && copveh.Speed < 2 && dist < 15)
                    {
                        if (OdotusLooppi(5))
                            break;
                        if (playerspeed < 1 && dist < 15)
                        {
                            ticket = true;
                            break;
                        }
                    }
                    /*      if (copveh.HasCollidedWithAnything)
                          {
                              if (Game.Player.WantedLevel == 1)
                                  Game.Player.WantedLevel = 0;
                          }*/

                    if (ticks >= 3000)
                    {
                        Game.Player.WantedLevel = 1;
                        break;
                    }
                    if (!Game.Player.Character.IsInVehicle())
                    {
                        Game.Player.WantedLevel = 1;
                        break;
                    }
                    if (dist < 30 && ticks % 600 == 0)
                    {
                        if (ticks >= 2400)
                        {
                            Function.Call(Hash.BLIP_SIREN, copveh);
                            Wait(500);
                            Function.Call(Hash._0x3523634255FC3318, copped, "STOP_VEHICLE_CAR_WARNING_MEGAPHONE", pedvoice, "SPEECH_PARAMS_FORCE", 0);
                        }
                        else
                        {
                            Function.Call(Hash.BLIP_SIREN, copveh);
                            Wait(500);
                            Function.Call(Hash._0x3523634255FC3318, copped, "STOP_VEHICLE_GENERIC_MEGAPHONE", pedvoice, "SPEECH_PARAMS_FORCE", 0);
                        }
                    }
                }

                else if (dist > 300)
                    break;

                Wait(0);
            }

            if (ticket)
            {
                TicketTime ttime = new TicketTime();
                ttime.Ticketer();
            }


            else
            {
                copped.Task.ClearAll();
                EndEvent();
            }
        }
    }
}
