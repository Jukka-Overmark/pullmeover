﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;

using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace PullMeOver
{
    public class PullMeOverMain : Script
    {
        enum CopStates { None, Chase, Follow, Ticket, StepOut };
        static CopStates state;
        enum EventState { Locate, None };
        EventState eventstate = EventState.None;

        float playerspeed;
        float finalplayerspeed;
        float speedlimit;
        float wlimit;
        float stopsignspeed = 100;
        float speedingdifference = 0;
        float speedlimitatcaught;

        bool scriptenabled = true;
        bool keyupped;
        bool stoppedforcarfix;
        bool redlight;
        bool blips;
        bool enterehjadriverside;
        bool enterehjapassengerside;
        bool jacking;
        bool break_in;
        bool stoppedforhelmet;
        bool drivingstolenvehicle;
        bool settingstolenvehicle;
        bool settingnotifystolen;
        bool settingwithouthelmet;
        bool settingwheelie;
        bool settingagainsttraffic;
        bool settingcolliding;
        bool settingnotrw;
        bool settingmobilephone;
        bool settingtailgating;
        bool settingdrifting;
        bool settingonsidewalk;
        bool settingcanbereported;
        bool settingstopsign;
        bool settingcopcarchange;
        bool settinglicensesuspension;
        bool settingburnout;
        bool settingwitnessblips;
        bool settingapproachwithlights;
        bool wasblipped;
        bool waschanged;
        bool licensesuspended;
        bool debugss;
        bool debugv;

        Vector3 finalstop;
        Vector3 prevpos;

        Vehicle[] vehs;
        Vehicle copveh;
        public Vehicle playerVehicle;
        Vehicle enterVehicle;
        Vehicle jackedvehicle;
        public static Vehicle fleevehicle;

        Random rand = new Random();

        int keycount = 0;
        int ticks;
        int againsttrafficticks = 0;
        int tailgatingticks = 0;
        int slidingticks = 0;
        public int range;
        int hwaylimit;
        int generallimit;
        int wantedspeedlimit;
        int burnoutticks = 0;
        int ticktimer = 0;
        int wheelieticks = 0;
        int settingmaxviolations;
        int settingexpireminutes;
        int aikanyt = DateTime.Now.Second;
        int[] addonpeds = { 2119533924, 1388307514, -1391788292, -1713450450, -560354151, -482908958, 1481343398, 640852342, 2017510805, -1883678939 };
        int[] liikennevalot = { 1043035044, 865627822, 862871082, -655644382 };
        int language = 0;

        string rikepaikka;
        string atrikepaikka;
        string currentstreet;
        string[] freeways = { "Los Santos Freeway", "Del Perro Freeway", "Great Ocean Highway", "La Puerta Freeway", "Elysian Fields Freeway", "Palomino Freeway", "Senora Freeway",
                          "Los Santos Fwy", "Del Perro Fwy", "Great Ocean Highway", "Great Ocean Hwy", "La Puerta Fwy", "Elysian Fields Fwy", "Palomino Fwy", "Senora Fwy", "Olympic Fwy"};
        string[] ticketyells = { "CHALLENGE_THREATEN", "CHAT_STATE", /*"CRIMINAL_WARNING",*/ "PROVOKE_STARING", "WON_DISPUTE", "GENERIC_INSULT_MED" };
        string[] rus = {    "Вы привысили скорость на даном участке дороги",
                            "Ваше вождение ставит под угрозу пешеходов!",
                            "Вы двигались по встречной полосе",
                            "Вы допустили столкновение.",
                            "Ваше авто имеет сильные повреждения. Пожалуйста, почините его.",
                            "Что это было в ваших руках? Телефон? Запрещено управлять ТС с телефоном в руках",
                            "Вы не соблюли дистанцию между ТС",
                            "Кажется, у вас проблемы с управлением вашим автомобилем",
                            "Вы проехали на красный свет!",
                            "Всегда надевайте шлем перед началом движения. Спасибо.",
                            "Где ваш шлем?!",
                            "Это не гоночный трек!",
                            "Вы не остановились на знаке STOP",
                            "Также...",
                            "~r~Вы не остановились на требование полицейского.",
                            "~r~У вас просрочена лицензия на управление транспортными средствами",
                            "~o~Подождите...",
                            "Ваше транспортное средство было изъято. Вам запрещено управлять данным ТС",
                            "~o~У вас слишком много нарушений ПДР. Я вынужден \nанулировать вашу лицензию водителя",
                            "~o~Ваша лицензия приостановлена. Пожалуйста, выйдите из машины."
                };

        Ped copped;
        Ped newcop;
        Ped jackeddriver;

        ScriptSettings config;
        Keys enablescript;

        Blip copblip;

        Prop stopsign;

        Dictionary<int, bool> stolenlist;

        List<string> offences = new List<string>();
        List<Vehicle> autolista = new List<Vehicle>();
        List<Record> recordlista = new List<Record>();
        List<Vehicle> haltulista = new List<Vehicle>();
        List<Vehicle> evasionlista = new List<Vehicle>();
        List<Record> felonylista = new List<Record>();
        protected static List<BlipHandler> bliplista = new List<BlipHandler>();
        List<WitnessHandler> witnesslista = new List<WitnessHandler>();
        static List<CopsSentHandler> backuplista = new List<CopsSentHandler>();
        static List<Ped> backuppedit = new List<Ped>();

        public PullMeOverMain()
        {
            Tick += OnTick;
            KeyDown += OnKeyDown;
            KeyUp += OnKeyUp;
            Aborted += OnAborted;
            Interval = 5;
            ticks = 0; 
            config = ScriptSettings.Load("scripts/PullMeOver.ini");
            enablescript = config.GetValue<Keys>("Settings", "Key", Keys.F11);
            hwaylimit = config.GetValue<int>("Settings", "Highway speed limit", 75);
            generallimit = config.GetValue<int>("Settings", "General speed limit", 50);
            wantedspeedlimit = config.GetValue<int>("Settings", "Exceed speed limit by", 50);
            range = config.GetValue<int>("Settings", "Range", 75);
            redlight = config.GetValue<bool>("Violations", "Running a red light", true);
            blips = config.GetValue<bool>("Settings", "Police car blips", true);
            settingstolenvehicle = config.GetValue<bool>("Crimes", "Stolen vehicles can be reported", false);
            settingnotifystolen = config.GetValue<bool>("Crimes", "Stolen vehicle notifications", false);
            settingwithouthelmet = config.GetValue<bool>("Violations", "Without helmet", true);
            settingwheelie = config.GetValue<bool>("Violations", "Wheelie/Stoppie", true);
            settingagainsttraffic = config.GetValue<bool>("Violations", "Driving against traffic", true);
            settingcolliding = config.GetValue<bool>("Violations", "Colliding ", true);
            settingnotrw = config.GetValue<bool>("Violations", "Heavily damaged vehicle", true);
            settingmobilephone = config.GetValue<bool>("Violations", "Using a mobile phone", true);
            settingtailgating = config.GetValue<bool>("Violations", "Tailgating ", true);
            settingdrifting = config.GetValue<bool>("Violations", "Drifting", true);
            settingonsidewalk = config.GetValue<bool>("Violations", "Driving on sidewalk", true);
            settingcanbereported = config.GetValue<bool>("Settings", "Can be reported", true);
            settingmaxviolations = config.GetValue<int>("Settings", "Max", 7);
            settingexpireminutes = config.GetValue<int>("Settings", "Minutes", 15);
            settingstopsign = config.GetValue<bool>("Violations", "Stop sign", true);
            settingburnout = config.GetValue<bool>("Violations", "Burnout/Excess revving", true);
            settingcopcarchange = config.GetValue<bool>("Settings", "Allow changing pursuing cop car", true);
            settinglicensesuspension = config.GetValue<bool>("Settings", "License suspension", true);
            settingwitnessblips = config.GetValue<bool>("Crimes", "Witness blips", false);
            settingapproachwithlights = config.GetValue<bool>("Settings", "Lights on when near", false);
            debugss = config.GetValue<bool>("Debug", "Debugss", false);
            debugv = config.GetValue<bool>("Debug", "Debugv", false);
            stolenlist = new Dictionary<int, bool>();
            wlimit = wantedspeedlimit / 2.236936f;
            language = Function.Call<int>(Hash._GET_UI_LANGUAGE_ID);

        }

        void OnTick(object sender, EventArgs e)
        {
            ticktimer++;
            TrackPlayer();
            Tickers();
        }

        void Tickers()
        {
            if(aikanyt != DateTime.Now.Second)
            {   
                aikanyt = DateTime.Now.Second;
                int lkm = recordlista.Count;
                int flkm = felonylista.Count;

                for (int i = 0; i < bliplista.Count; i++)
                {
                    if (bliplista[i].Rauennut())
                        bliplista.Remove(bliplista[i]);
                }
                for (int i = 0; i < recordlista.Count; i++)
                {
                    if (recordlista[i].Rauennut())
                    {
                        recordlista.Remove(recordlista[i]);
                    }     
                }
                for (int i = 0; i < felonylista.Count; i++)
                {
                    if (felonylista[i].Rauennut())
                    {
                        felonylista.Remove(felonylista[i]);
                    }
                }

                if (felonylista.Count == 0 && licensesuspended && recordlista.Count < settingmaxviolations)
                {
                    for (int i = 0; i < haltulista.Count; i++)
                    {
                        haltulista[i].LockStatus = VehicleLockStatus.Unlocked;
                    }

                    UI.Notify("Your license was reinstated");
                    UI.Notify("~g~You may now use your vehicles");
                    licensesuspended = false;
                }

                for (int i = 0; i < witnesslista.Count; i++)
                {
                    if (witnesslista[i].ped.IsDead)
                        witnesslista[i].ped.CurrentBlip.Remove();

                    if (witnesslista[i].Rauennut())
                    {
                        if (witnesslista[i].ped.IsAlive)
                        {
                            int hashi = witnesslista[i].autohash;
                            if (stolenlist.Keys.Contains(hashi))
                            {
                                stolenlist[hashi] = true;
                                if(Game.Player.Character.IsInVehicle())
                                {
                                    if(playerVehicle.GetHashCode() == hashi)
                                        UI.Notify("~r~This vehicle has been reported stolen");
                                    drivingstolenvehicle = true;
                                }
                               
                            }     
                        }
                        witnesslista[i].ped.IsPersistent = false;
                    }
                }
                    
            }
        }

        void OnAborted(object sender, EventArgs e)
        {
            for (int i = 0; i < haltulista.Count; i++)
            {
                haltulista[i].LockStatus = VehicleLockStatus.Unlocked;
                if (Function.Call<bool>(Hash.IS_VEHICLE_ATTACHED_TO_TRAILER, haltulista[i]))
                {
                    OutputArgument trailer = new OutputArgument();
                    Function.Call<Entity>(Hash.GET_VEHICLE_TRAILER_VEHICLE, haltulista[i], trailer);
                    Entity temp = trailer.GetResult<Entity>();

                    if (!temp.Equals(null))
                    {
                        temp.IsPersistent = false;
                    }
                }
                haltulista[i].IsPersistent = false;
            }
            for (int i = 0; i < bliplista.Count; i++){bliplista[i].Raukea();}
            EndEvent(false);
        }


        void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == enablescript && !keyupped)
            {
                keycount++;
                if (keycount >= 2)
                {
                    keyupped = true;
                    scriptenabled = !scriptenabled;
                    if (scriptenabled)
                        UI.Notify("PullMeOver ~g~ENABLED");
                    else
                        UI.Notify("PullMeOver ~r~DISABLED");
                    keycount = 0;
                }
            }
            if (e.Shift && e.KeyCode == enablescript)
            {
                if(recordlista.Count > 0)
                    UI.Notify("~y~"+recordlista.Count+"/"+settingmaxviolations + " ~w~Violations");
                else
                    UI.Notify(recordlista.Count + "/" + settingmaxviolations + " Violations");
                if(felonylista.Count > 1)
                    UI.Notify("~r~"+felonylista.Count + " ~w~Felonies");
                else if(felonylista.Count == 1)
                    UI.Notify("~r~" + felonylista.Count + " ~w~Felony");
                else
                    UI.Notify("~w~" + felonylista.Count + " Felonies");
                if (licensesuspended)
                    UI.Notify("~r~License is suspended");
                else
                    UI.Notify("~g~License is valid");
            }
        }

        bool GetCopInVehicle(int rangex, bool cansee, bool chasechange, bool invehicleonly)
        {
            List<Ped> pedilista = new List<Ped>();
            List<Ped> witnesslista = new List<Ped>();

            Ped[] copsi = World.GetNearbyPeds(Game.Player.Character.Position, rangex);

            for (int i = 0; i < copsi.Length; i++)
            {
                if (Function.Call<int>(Hash.GET_PED_TYPE, copsi[i]) == 6 || addonpeds.Contains(copsi[i].Model.Hash))
                {
                    if (!copsi[i].Equals(Game.Player.Character))
                    {
                        if (copsi[i].IsInVehicle())
                        {
                            Vehicle tempveh = copsi[i].CurrentVehicle;
                            if (tempveh.Model.IsCar || tempveh.Model.IsBike)
                            {
                                if (!tempveh.SirenActive)
                                {
                                    if (tempveh.GetPedOnSeat(VehicleSeat.Driver).Equals(copsi[i]))
                                    {
                                        if (Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, copsi[i].Handle, Game.Player.Character.Handle, 17) || !cansee)
                                            pedilista.Add(copsi[i]);
                                    }
                                }
                            }
                        }
                        else
                            witnesslista.Add(copsi[i]);
                    }
                }
            }

            if (pedilista.Count > 0)
            {
                float maxdist = 1000;
                for (int i = 0; i < pedilista.Count; i++)
                {
                    if (Vector3.Distance(pedilista[i].Position, Game.Player.Character.Position) < maxdist)
                    {
                        maxdist = Vector3.Distance(pedilista[i].Position, Game.Player.Character.Position);
                        if (!chasechange)
                            copped = pedilista[i];
                        else
                            newcop = pedilista[i];
                    }
                }
                return true;
            }
            else if (witnesslista.Count > 0 && cansee && !chasechange && !invehicleonly && settingcanbereported)
            {
                float maxdist = 40;
                Ped witness = null;
                bool loyty = false;
                for (int i = 0; i < witnesslista.Count; i++)
                {
                    if (Vector3.Distance(witnesslista[i].Position, Game.Player.Character.Position) < maxdist)
                    {
                        maxdist = Vector3.Distance(witnesslista[i].Position, Game.Player.Character.Position);
                        witness = witnesslista[i];
                        Function.Call(Hash._PLAY_AMBIENT_SPEECH2, witness, "SUSPECT_SPOTTED", "SPEECH_PARAMS_STANDARD");
                        bliplista.Add(new BlipHandler(witness, DateTime.Now.AddSeconds(6), true));
                           
                        loyty = true;
                    }
                }
                if(loyty && witness.IsOnFoot)
                {
                    witness.Task.TurnTo(Game.Player.Character, 6000);
                    if (GetCopInVehicle(400, false, false, false))
                        return true;
                }
                
                return false;
            }
            return false;
        }

        void SetChase()
        {
            copveh = copped.CurrentVehicle;
            state = CopStates.Chase;

            copped.IsPersistent = true;
            
            if (blips)
            {
                Blip temppi = copveh.CurrentBlip;
                if (!temppi.IsOnMinimap)
                    copveh.AddBlip();

                copblip = copveh.CurrentBlip;
                copblip.Sprite = BlipSprite.PoliceCarDot;
                copblip.Scale = 0.5f;
            }

            copped.BlockPermanentEvents = true;
         //   pedvoice = getCopVoice(copped);
            Action();
        }

        bool ChaserCanSee()
        {
            if (Vector3.Distance(copped.Position, Game.Player.Character.Position) < range)
            {
                if (Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, copped.Handle, Game.Player.Character.Handle, 17))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        bool IsInterrupted()
        {
            if (!copped.Exists())
            {
                EndEvent(false);
                return true;
            }
            if (copped.IsDead || Game.Player.Character.IsDead)
            {
                EndEvent(false);
                return true;
            }
            if (Game.Player.WantedLevel > 0)
            {
                if (copped.IsAlive && !copped.IsInVehicle())
                {
                    copped.Task.ClearAllImmediately();
                    copped.BlockPermanentEvents = false;
                    copped.Task.FightAgainst(Game.Player.Character, 2000);
                }
                EndEvent(false);
                return true;
            }

            if (state == CopStates.StepOut)
            {
                if(Game.Player.Character.IsInVehicle() && Vector3.Distance(copped.Position, Game.Player.Character.Position) > 15)
                {   
                    Game.Player.WantedLevel = 1;
                    EndEvent(false);
                    return true;  
                }
                if(!Game.Player.Character.IsInVehicle())
                {
                    playerVehicle.IsPersistent = true;

                    if (Function.Call<bool>(Hash.IS_VEHICLE_ATTACHED_TO_TRAILER, playerVehicle))
                    {
                        OutputArgument trailer = new OutputArgument();
                        Function.Call<Entity>(Hash.GET_VEHICLE_TRAILER_VEHICLE, playerVehicle, trailer);
                        Entity temp = trailer.GetResult<Entity>();

                        if (!temp.Equals(null))
                            temp.IsPersistent = true;
                    }

                    playerVehicle.LockStatus = VehicleLockStatus.LockedForPlayer;
                    playerVehicle.EngineRunning = false;
                    haltulista.Add(playerVehicle);
                    return true;
                }
                return false;
            }
            if (!Game.Player.Character.IsInVehicle())
            {
                if(state == CopStates.Chase || state == CopStates.Follow)
                {
                    if (Vector3.Distance(copped.Position, Game.Player.Character.Position) <= 350)
                    {
                        if (!Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, playerVehicle.Handle, copped.Handle, 17))
                        {
                            eventstate = EventState.Locate;
                            return true;
                        }
                        else
                        {
                            Game.Player.WantedLevel = 1;
                            EndEvent(false);
                            return true;
                        }  
                    }

                }
            }

            if (state == CopStates.Ticket)
            {
                if (Game.Player.Character.IsInVehicle())
                {
                    if (Vector3.Distance(finalstop, Game.Player.Character.Position) > 5)
                    {
                        copped.Task.ClearAllImmediately();
                        copped.BlockPermanentEvents = false;
                        copped.Task.EnterVehicle(copveh, VehicleSeat.Driver);
                        Game.Player.WantedLevel = 1;
                        EndEvent(false);
                        return true;
                    }
                }
                else if (!Game.Player.Character.IsInVehicle())
                {
                    copped.Task.ClearAllImmediately();
                 //   copped.Task.EnterVehicle(copveh, VehicleSeat.Driver);
                    copped.BlockPermanentEvents = false;
                    copped.Task.Arrest(Game.Player.Character);//.FightAgainst(Game.Player.Character, 2000);
                    Game.Player.WantedLevel = 1;
                    EndEvent(false);
                    return true;
                }
            }

            if (state != CopStates.Ticket)
                TrackPlayer();

            return false;
        }

        void ExcessSpeeding(int rangex, int cticks)
        {
            if (Game.Player.WantedLevel == 0)
            {
                Ped[] cops = World.GetNearbyPeds(Game.Player.Character.Position, rangex);

                for (int i = 0; i < cops.Length; i++)
                {
                    if (Function.Call<int>(Hash.GET_PED_TYPE, cops[i]) == 6 || addonpeds.Contains(cops[i].Model.Hash))
                    {
                        if (!cops[i].Equals(Game.Player.Character))
                        {
                            if (!Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, cops[i].Handle, Game.Player.Character.Handle, 17))
                                return;

                            if ((playerVehicle.Speed - speedlimit) >= wlimit)
                                Game.Player.WantedLevel = 1;
                        }
                    }
                }
            }
        }

        void Action()
        {
            while (!IsInterrupted())
            {
                if (!Function.Call<bool>(Hash.IS_PED_FACING_PED, Game.Player.Character, copped, 100f))
                    break;
                else if (Vector3.Distance(copped.Position, Game.Player.Character.Position) > (range + 50) && playerVehicle.Speed < copveh.Speed)
                    break;
                else if (waschanged)
                    break;

                Wait(0);
            }

            if (IsInterrupted())
                return;

            int trailr = 0;
            if (Function.Call<bool>(Hash.IS_VEHICLE_ATTACHED_TO_TRAILER, playerVehicle))
            {
                if (playerVehicle.Model.Equals(VehicleHash.Bison) ||
                    playerVehicle.Model.Equals(VehicleHash.Bison2) ||
                    playerVehicle.Model.Equals(VehicleHash.Bison3) ||
                    playerVehicle.Model.Equals(VehicleHash.Sadler) ||
                    playerVehicle.Model.Equals(VehicleHash.Sadler2))
                {
                    trailr = 0;
                }
                else
                    trailr = 10;
            }

            Vehicle playv = playerVehicle;
            float tspeed = playv.Speed;
            //      Function.Call(Hash._TASK_VEHICLE_FOLLOW, copped, copveh, playv.Handle, 6, 50, 20);
            Function.Call(Hash.TASK_VEHICLE_CHASE, copped, Game.Player.Character.Handle);
            Function.Call(Hash.SET_TASK_VEHICLE_CHASE_IDEAL_PURSUIT_DISTANCE, copped, 50f);
            Function.Call(Hash.SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG, copped, 32, true);
            Function.Call(Hash.SET_DRIVER_ABILITY, copped, 0.9f);

            copped.Weapons.RemoveAll();

            if(!settingapproachwithlights || waschanged)
            {
                Function.Call(Hash.DISABLE_VEHICLE_IMPACT_EXPLOSION_ACTIVATION, copveh, true);
                copveh.SirenActive = true;
            }
            

            bool ticket = false;
            bool lights = false;

            float dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
            ticks = 0;
            int visualticks = 0;
            bool changecop = false;
            
            while (!IsInterrupted())
            {
                
                dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
                tspeed = playv.Speed;
                playerspeed = playerVehicle.Speed;

                ExcessSpeeding(100, 0);
                
              //  UI.ShowSubtitle(dist + " ");

                if (!Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, copped.Handle, Game.Player.Character.Handle, 17))
                {
                    visualticks++;
                    if (visualticks > 2000 && dist > 100)
                        break;
                }
                else
                    visualticks = 0;

                if(settingcopcarchange && ((dist > 100 && copveh.Speed < 20) || dist > 150))
                {
                    if (GetCopInVehicle(30, true, true, true))
                    {
                        waschanged = true;
                        changecop = true;
                        break; 
                    }
                }
                
                if (dist < 100)
                {
                    ticks++;
                    if (playerspeed == 0)
                        ticks--;

                    if (dist < 35+trailr && !lights && Function.Call<bool>(Hash.IS_PED_FACING_PED, copped, Game.Player.Character, 90f) && !waschanged && ChaserCanSee())
                    {
                        if(settingapproachwithlights)
                        {
                            Function.Call(Hash.DISABLE_VEHICLE_IMPACT_EXPLOSION_ACTIVATION, copveh, true);
                            copveh.SirenActive = true;
                        }
                          
                        Function.Call(Hash._PLAY_AMBIENT_SPEECH2, Game.Player.Character, "GENERIC_CURSE_MED", "SPEECH_PARAMS_FORCE");
                        Function.Call(Hash.BLIP_SIREN, copveh);
                        wasblipped = true;
                        lights = true;
                    }
                    if (dist >= 20+trailr && state != CopStates.Chase)
                    {
                        state = CopStates.Chase;
                        Function.Call(Hash.TASK_VEHICLE_CHASE, copped, Game.Player.Character.Handle);
                 //       Function.Call(Hash.SET_TASK_VEHICLE_CHASE_IDEAL_PURSUIT_DISTANCE, copped, 50f);
                 //       Function.Call(Hash.SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG, copped, 1, true);
                        Function.Call(Hash.SET_DRIVER_ABILITY, copped, 0.9f);
                        Function.Call(Hash.SET_TASK_VEHICLE_CHASE_IDEAL_PURSUIT_DISTANCE, copped, 20);
                        Function.Call(Hash.SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG, copped, 32, true);

                        copped.Weapons.RemoveAll();
                    }
                    else if (dist < 20 + trailr && dist >= 12+trailr && state != CopStates.Follow)
                    {
                        state = CopStates.Follow;
                        Function.Call(Hash._TASK_VEHICLE_FOLLOW, copped, copveh, playv.Handle, 100, 0, 10+trailr);
                    }
                    else if (dist < 12 + trailr && playerspeed < 1 && copveh.Speed < 2)
                    {
                        int tticks = 0;;
                        while (playerVehicle.Speed < 1 && !IsInterrupted() && (dist < 12 + trailr) && tticks < 5)
                        {
                            Wait(1000);
                            tticks++;  
                        }
                        if (tticks >= 5)
                        {
                            ticket = true;
                            break;
                        }
                    }
                    
                    if (dist < 7 && copveh.Speed > playerVehicle.Speed && playerVehicle.Speed < 3)
                        copveh.Speed = 0f;

                    if (ticks >= 3000)
                    {
                        Game.Player.WantedLevel = 1;
                        break;
                    }

                    if (dist < 30 && ticks % 600 == 0)
                    {
                        if (ticks >= 2400)
                        {
                            Function.Call(Hash.BLIP_SIREN, copveh);
                            Wait(500);
                            Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "STOP_VEHICLE_CAR_WARNING_MEGAPHONE", "SPEECH_PARAMS_FORCE");
                        }
                        else
                        {
                            Function.Call(Hash.BLIP_SIREN, copveh);
                            wasblipped = true;
                            Wait(500);
                            Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "STOP_VEHICLE_GENERIC_MEGAPHONE", "SPEECH_PARAMS_FORCE");
                        }
                    }
                }

                else if (dist > 350)
                    break;

                Wait(0);
            }

            if (ticket)
                TicketTime(copveh, copped);
            else if (eventstate == EventState.Locate)
                SendCops();
            else if (changecop)
            {
                EndEvent(true);
                SetChase();
            }

            else
            {
                if (wasblipped)
                    evasionlista.Add(playerVehicle);
                copped.Task.ClearAll();
                EndEvent(false);
            }
        }

        Vector3 PlayaPosition()
        {
            Vector3 pposi;
            Vehicle vehi = playerVehicle;
            Game.Player.Character.Task.WarpOutOfVehicle(vehi);
            Wait(0);
            pposi = Game.Player.Character.Position;
            Game.Player.Character.Task.WarpIntoVehicle(vehi, VehicleSeat.Driver);
            return pposi;
        }

        void Arrest()
        {
            finalstop = Game.Player.Character.Position;
           // SendCops();
            if (OdotusLooppi(2))
                return;

            copveh.LockStatus = VehicleLockStatus.Locked;
            copped.Task.LeaveVehicle(copveh, false);
            
            copped.Task.ClearAll();
            copped.Weapons.Give(WeaponHash.StunGun, 1, false, true);
            copped.Weapons.Give(WeaponHash.Pistol, 120, true, true);

            World.SetRelationshipBetweenGroups(Relationship.Hate, copped.RelationshipGroup, Game.Player.Character.RelationshipGroup);
            World.SetRelationshipBetweenGroups(Relationship.Hate, Game.Player.Character.RelationshipGroup, copped.RelationshipGroup);

            if (OdotusLooppi(1))
                return;
            copped.Task.AimAt(Game.Player.Character, -1);
            Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "CHALLENGE_THREATEN", "SPEECH_PARAMS_SHOUTED_CLEAR");

            UI.ShowSubtitle("Cop: ~o~Do not move!", 8000);
            if (OdotusLooppi(10))
                return;

            Vector3 playpos = StoppedPlayerPostion();
            Function.Call(Hash.TASK_GOTO_ENTITY_AIMING, copped, Game.Player.Character.Handle, 1.5f, 30f);
            ticks = 0;
            while (!IsInterrupted())
            {
              
                float dist = Vector3.Distance(playpos, copped.Position);
                float dist2 = Vector3.Distance(Game.Player.Character.Position, finalstop);
                ticks++;

                if (dist2 > 5 || !Game.Player.Character.IsInVehicle())
                {
                    copped.Task.ClearAll();
                    Game.Player.WantedLevel = 1;
                    break;
                }

                if (dist < 1f)
                {
                    break;
                }
                else if (dist < 2.5f && ticks > 1500)
                    break;

                if (ticks > 3000)
                    break;

                Wait(0);
            }

            copped.Weapons.Give(WeaponHash.StunGun, 1, true, true);
            Function.Call(Hash.TASK_ARREST_PED, copped, Game.Player.Character);
            if (OdotusLooppi(4))
                return;
            EndEvent(false);
        }

        Vector3 StoppedPlayerPostion()
        {
            if (playerVehicle.Model.IsCar)
            {
                if (playerVehicle.IsDoorBroken(VehicleDoor.FrontLeftDoor))
                    return PlayaPosition();
                else if (playerVehicle.HasBone("seat_dside_f") && playerVehicle.HasBone("seat_pside_f"))
                {
                    Vector3 vector = playerVehicle.GetBoneCoord("seat_dside_f") - playerVehicle.GetBoneCoord("seat_pside_f");
                    return playerVehicle.GetBoneCoord("seat_dside_f") + vector;
                }

                else
                    return PlayaPosition();
            }
            else if (playerVehicle.Model.IsBicycle || playerVehicle.Model.IsBike)
            {
                Vector3 pos4 = Vector3.Cross(Game.Player.Character.ForwardVector, -Game.Player.Character.UpVector);
                return playerVehicle.Position + pos4;
            }
            else
                return PlayaPosition();
        }

        void TicketTime(Vehicle copveh, Ped copped)
        {
            
            state = CopStates.Ticket;
            if (offences.Contains("stolen"))
            {
                Arrest();
                return;
            }

            copveh.LockStatus = VehicleLockStatus.Locked;
            copped.Task.LeaveVehicle(copveh, true);

            finalstop = Game.Player.Character.Position;

            float dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
            float dist2;
            Vector3 playpos = StoppedPlayerPostion();

            Wait(1);
            copped.Task.GoTo(playpos, false, -1);
            ticks = 0;
            while (!IsInterrupted())
            {
                //Function.Call(Hash.DRAW_LINE, copped.Position.X, copped.Position.Y, copped.Position.Z, playpos.X, playpos.Y, playpos.Z, 255, 0, 0, 255);
                dist = Vector3.Distance(playpos, copped.Position);
                dist2 = Vector3.Distance(Game.Player.Character.Position, finalstop);
                ticks++;

                if (dist2 > 5 || !Game.Player.Character.IsInVehicle())
                {
                    copped.Task.ClearAll();
                    Game.Player.WantedLevel = 1;
                    break;
                }

                if (dist < 1f || (dist < 2.5f && copped.Velocity == Vector3.Zero))
                    break;

                if (ticks > 3000)
                    break;
                Wait(0);
            }

            if (IsInterrupted()) return;
            if (ticks > 3000)
            {
                EndEvent(false);
                UI.Notify("~r~Pullover aborted");
                return;
            }

            copped.Task.TurnTo(Game.Player.Character, 1500);
            Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_HI", "SPEECH_PARAMS_FORCE");
            while (Function.Call<bool>(Hash.IS_ANY_SPEECH_PLAYING, copped))
            {
                Wait(0);
            }

            if (OdotusLooppi(1))
                return;
            bool arrest = false;

            if (offences.Contains("sliding") && offences.Contains("burnout"))
                offences.Remove("burnout");

            int olkm = offences.Count;
            int rlkm = recordlista.Count;
            int flkm = felonylista.Count;
            bool isevasionvehicle = evasionlista.Contains(playerVehicle);
            if (!arrest)
            {
                Function.Call(Hash._PLAY_AMBIENT_SPEECH2, Game.Player.Character, "GENERIC_HOWS_IT_GOING", "SPEECH_PARAMS_STANDARD");

                if (OdotusLooppi(1))
                    return;

                Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "KIFFLOM_GREET", "SPEECH_PARAMS_FORCE");
                Function.Call(Hash.TASK_START_SCENARIO_AT_POSITION, copped, "CODE_HUMAN_MEDIC_TIME_OF_DEATH", copped.Position.X,
                    copped.Position.Y, copped.Position.Z, copped.Heading, offences.Count - 1 * 8000, false, false);

                if (OdotusLooppi(2))
                    return;

                for (int i = 0; i < offences.Count; i++)
                {
                    if (offences[i].Equals("speeding"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: You were doing ~r~" + (int)(finalplayerspeed * 2.236936f) + " ~w~in a " + (int)(speedlimitatcaught * 2.236936f) +
                            " MPH zone on ~g~" + rikepaikka + "~w~!", 8000);
                        else
                            UI.ShowSubtitle(rus[0], 8000);
                        //   Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("onpavement"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: Your driving is endangering pedestrians!", 8000);
                        else
                            UI.ShowSubtitle(rus[1], 8000);
                        //    Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("againsttraffic"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: You were driving against traffic on ~g~" + atrikepaikka + "~w~!", 8000);
                        else
                            UI.ShowSubtitle(rus[2], 8000);
                        //   Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("collision"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: That was quite a collision you had there.", 8000);
                        else
                            UI.ShowSubtitle(rus[3], 8000);
                        //   Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("notrw"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: Your vehicle is heavily damaged. Please get it fixed.", 8000);
                        else
                        {
                            UI.ShowSubtitle("Ваше авто имеет сильные повреждения", 4000);
                            if (OdotusLooppi(3))
                                return;
                            UI.ShowSubtitle("Пожалуйста, почините его.", 4000);
                        }
                        //    UI.ShowSubtitle(rus[4], 8000);
                        //   Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("phone"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: What was that in your hands? A phone? You know that's a traffic violation", 8000);
                        else
                        {
                            UI.ShowSubtitle("Что это было в ваших руках? Телефон?", 4000);
                            if (OdotusLooppi(3))
                                return;
                            UI.ShowSubtitle("Запрещено управлять ТС с телефоном в руках", 4000);
                        }
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("tailgating"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: You were driving really close to other vehicle", 8000);
                        else
                            UI.ShowSubtitle(rus[6], 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("sliding"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: You seem to have trouble controlling your vehicle", 8000);
                        else
                            UI.ShowSubtitle(rus[7], 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("redlight"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: You ran a red light!", 8000);
                        else
                            UI.ShowSubtitle(rus[8], 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("helmet"))
                    {
                        if (language != 7)
                        {
                            if (Game.Player.Character.IsWearingHelmet)
                                UI.ShowSubtitle("Cop: Oh, now you found your helmet", 8000);
                            else
                                UI.ShowSubtitle("Cop: Where is your helmet?!", 8000);
                        }
                        else
                        {
                            if (Game.Player.Character.IsWearingHelmet)
                                UI.ShowSubtitle(rus[9], 8000);
                            else
                                UI.ShowSubtitle(rus[10], 8000);
                        }
                            
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("wheelie"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: This is not a stunt track!", 8000);
                        else
                            UI.ShowSubtitle(rus[11], 8000);
                        //  Wait(1500);                       
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("stopsign"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: You didn't come to a stop at a stop sign", 8000);
                        else
                            UI.ShowSubtitle(rus[12], 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                   /* else if (offences[i].Equals("burnout"))
                    {
                        if (language != 7)
                            UI.ShowSubtitle("Cop: We get it, you have a powerful engine", 8000);
                        else
                            UI.ShowSubtitle(rus[1], 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }*/

                    if (OdotusLooppi(6))
                        break;

                    if (i != offences.Count - 1 || isevasionvehicle || recordlista.Count >= settingmaxviolations || felonylista.Count > 0)
                    {

                        if (IsInterrupted())
                            break;
                        if (language != 7)
                            UI.ShowSubtitle("Cop: Also...", 8000);
                        else
                            UI.ShowSubtitle(rus[13], 8000);
                        if (OdotusLooppi(4))
                            break;
                    }
                }
            }
            

            if(isevasionvehicle)
            {
                if (language != 7)
                    UI.ShowSubtitle("Cop: ~r~You shouldn't run from the police. This vehicle was used in a felony evasion", 8000);
                else
                    UI.ShowSubtitle(rus[14], 8000);
                evasionlista.Remove(playerVehicle);
                if (OdotusLooppi(6))
                    return;
            }
            else if (recordlista.Count >= settingmaxviolations || felonylista.Count > 0)
            {
                if (language != 7)
                    UI.ShowSubtitle("Cop: ~r~You are driving on suspended license", 8000);
                else
                {
                    UI.ShowSubtitle("~r~У вас просрочена лицензия", 4000);
                    if (OdotusLooppi(3))
                        return;
                    UI.ShowSubtitle("~r~на управление транспортными средствами", 4000);
                }
                if (OdotusLooppi(6))
                    return;
            }
                  
            LisaaRecordiin(olkm);
            if ((isevasionvehicle && settinglicensesuspension) || (licensesuspended && settinglicensesuspension))
            {
                felonylista.Add(new Record(DateTime.Now.AddMinutes(settingexpireminutes)));
                licensesuspended = true;
            }
            else if (recordlista.Count >= settingmaxviolations && settinglicensesuspension)
            {
                licensesuspended = true;
            }
            ticks = 0;
            /* if (arrest)
             {
                 
                 EndEvent();
             }*/

            if (Function.Call<bool>(Hash.IS_PED_USING_ANY_SCENARIO, copped))
            {
                copped.Task.ClearAll();
                if (OdotusLooppi(3))
                    return;
                if((recordlista.Count >= settingmaxviolations && settinglicensesuspension) || (felonylista.Count > 0 && settinglicensesuspension))
                {
                    if (language != 7)
                        UI.ShowSubtitle("Cop: ~o~Hold on...", 8000);
                    else
                        UI.ShowSubtitle(rus[16], 8000);
                }
                    
                if (OdotusLooppi(4))
                    return;
            }

            if (IsInterrupted()) { EndEvent(false); return; }

            if (!licensesuspended)
            {
                Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "CRIMINAL_WARNING", "SPEECH_PARAMS_FORCE");
                Wait(200);
                if (!Function.Call<bool>(Hash.IS_AMBIENT_SPEECH_PLAYING, copped))
                    Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_BYE", "SPEECH_PARAMS_FORCE");
            }
               
            for (int i = 0; i < offences.Count; i++)
            {
                if (offences[i].Equals("speeding"))
                {
                    int limit = (int)(speedlimitatcaught * 2.236936f);
                    int pspeed = (int)(finalplayerspeed * 2.236936f);

                    UI.Notify("You were fined ~r~$" + (pspeed - limit) * 20 + "~w~ for going " + (pspeed - limit) + " over the speed limit.");
                    Game.Player.Money = Game.Player.Money - (pspeed - limit) * 20;
                }
                else if (offences[i].Equals("onpavement"))
                {
                    UI.Notify("You were fined ~r~$350 ~w~for unsafe driving.");
                    Game.Player.Money = Game.Player.Money - 350;
                }
                else if (offences[i].Equals("againsttraffic"))
                {
                    UI.Notify("You were fined ~r~$250 ~w~for careless driving.");
                    Game.Player.Money = Game.Player.Money - 250;
                }
                else if (offences[i].Equals("collision"))
                {
                    UI.Notify("You were fined ~r~$500 ~w~for damage to property.");
                    Game.Player.Money = Game.Player.Money - 500;
                }
                else if (offences[i].Equals("notrw"))
                {
                    UI.Notify("You were fined ~r~$180 ~w~for unroadworthy vehicle.");
                    Game.Player.Money = Game.Player.Money - 180;
                    stoppedforcarfix = true;
                }
                else if (offences[i].Equals("phone"))
                {
                    UI.Notify("You were fined ~r~$100 ~w~for using a mobile phone while driving.");
                    Game.Player.Money = Game.Player.Money - 100;
                }
                else if (offences[i].Equals("tailgating"))
                {
                    UI.Notify("You were fined ~r~$100 ~w~for tailgating.");
                    Game.Player.Money = Game.Player.Money - 100;
                }
                else if (offences[i].Equals("sliding"))
                {
                    UI.Notify("You were fined ~r~$350 ~w~for reckless driving.");
                    Game.Player.Money = Game.Player.Money - 350;
                }
                else if (offences[i].Equals("redlight"))
                {
                    UI.Notify("You were fined ~r~$450 ~w~for running a red light.");
                    Game.Player.Money = Game.Player.Money - 450;
                }
                else if (offences[i].Equals("helmet"))
                {
                    UI.Notify("You were fined ~r~$200 ~w~for not wearing motorcycle helmet.");
                    Game.Player.Money = Game.Player.Money - 200;
                    stoppedforhelmet = true;
                }
                else if (offences[i].Equals("wheelie"))
                {
                    UI.Notify("You were fined ~r~$350 ~w~for reckless driving.");
                    Game.Player.Money = Game.Player.Money - 350;       
                }
                else if (offences[i].Equals("stopsign"))
                {
                    UI.Notify("You were fined ~r~$240 ~w~for a failure to stop at a stop sign.");
                    Game.Player.Money = Game.Player.Money - 240;
                }
                else if (offences[i].Equals("burnout"))
                {
                    UI.Notify("You were fined ~r~$140 ~w~for exhibition of power.");
                    Game.Player.Money = Game.Player.Money - 140;
                }
            }

            if ((recordlista.Count >= settingmaxviolations && settinglicensesuspension) || (felonylista.Count > 0 && settinglicensesuspension))
            {
                if(recordlista.Count >= settingmaxviolations)
                    UI.Notify("You have ~y~" + recordlista.Count + "/" + settingmaxviolations + " ~w~violations on your record");
                state = CopStates.StepOut;
                GetOut(playpos, olkm, flkm, rlkm);       
            }
            else
                UI.Notify("You have ~y~" + recordlista.Count + "/" + settingmaxviolations + " ~w~violations on your record");

            if (!Game.Player.Character.IsInVehicle() && Game.Player.WantedLevel == 0 && state == CopStates.StepOut)
            {
                if (language != 7)
                    UI.ShowSubtitle("Cop: Your vehicle has been seized. You are not allowed to drive", 6000);
                else
                {
                    UI.ShowSubtitle("Ваше транспортное средство было изъято", 3000);
                    Wait(3000);
                    UI.ShowSubtitle("Вам запрещено управлять данным ТС", 3000);
                }
                Wait(2000);
            }

            Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "CRIMINAL_WARNING", "SPEECH_PARAMS_FORCE");
            Wait(200);
            if (!Function.Call<bool>(Hash.IS_AMBIENT_SPEECH_PLAYING, copped))
                Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_BYE", "SPEECH_PARAMS_FORCE");
                 
            copped.Task.EnterVehicle(copveh, VehicleSeat.Driver);
            copped.BlockPermanentEvents = false;
            Wait(1000);
            EndEvent(false);

        }

        void GetOut(Vector3 playpos, int olkm, int flkm, int rlkm)
        {

            //copped.Task.GoTo(copveh.Position - playpos + playpos, false, -1);
            copped.Task.EnterVehicle(copveh, VehicleSeat.Driver);
            Wait(1500);
            copped.Task.ClearAll();           
            copped.Task.TurnTo(Game.Player.Character, -1);
            Wait(1500);
            Function.Call(Hash.TASK_START_SCENARIO_IN_PLACE, copped, "WORLD_HUMAN_COP_IDLES", 0, 1);
            Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_INSULT_HIGH", "SPEECH_PARAMS_FORCE");

            if((rlkm < settingmaxviolations) && (recordlista.Count >= settingmaxviolations) && (flkm == 0))
            {
                if (language != 7)
                    UI.ShowSubtitle("Cop: ~y~You have too many violations on record.\n~w~ Your license is now suspended. Please step out of the vehicle", 8000);
                else
                {
                    UI.ShowSubtitle("~o~У вас слишком много нарушений ПДР", 4000);
                    Wait(4000);
                    UI.ShowSubtitle("~o~Я вынужден анулировать вашу лицензию водителя", 4000);
                }
            }
                
          //  else if (flkm > 0)
          //      UI.ShowSubtitle("Cop: You have too many violations on record. ~o~Your license is now suspended. ~w~Please step out of the vehicle", 8000);
            else
            {
                if (language != 7)
                    UI.ShowSubtitle("Cop:~o~ Your license is suspended.~w~\n Please step out of the vehicle", 8000);
                else
                {
                    UI.ShowSubtitle("~o~Ваша лицензия приостановлена", 4000);
                    Wait(4000);
                    UI.ShowSubtitle("~o~Пожалуйста, выйдите из машины", 4000);
                }
            }
                

            while (!IsInterrupted())
            {
                
                Wait(0);
            } 
        }

        void Spiikki(string keissi)
        {
            string randi = "";

            while (!Function.Call<bool>(Hash.IS_ANY_SPEECH_PLAYING, copped))
            {
                randi = ticketyells[rand.Next(ticketyells.Length)];
                Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, randi, "SPEECH_PARAMS_FORCE");
                Wait(0);
            }
          //  UI.Notify(randi);
        }

        void OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == enablescript)
            {
                keyupped = false;
                keycount = 0;
            }
        }

        void EndEvent(bool change)
        {
            if (copped.Exists())
                if (copped.IsAlive)
                {
                    copped.Weapons.Give(WeaponHash.Pistol, 120, false, true);
                    copped.Weapons.Give(WeaponHash.StunGun, 1, false, true);
                    copped.Weapons.Give(WeaponHash.Nightstick, 1, false, true);
                    copped.BlockPermanentEvents = false;
                    copped.Task.ClearAll();
                }

            if (blips && copblip.Exists())
                copblip.Remove();

            if (Game.Player.WantedLevel > 0 && Game.Player.Character.IsInVehicle())
            {
                // Function.Call(Hash.SET_VEHICLE_IS_STOLEN, playerVehicle, false);
                if (stolenlist.ContainsKey(Game.Player.Character.CurrentVehicle.GetHashCode()))
                    stolenlist.Remove(Game.Player.Character.CurrentVehicle.GetHashCode());
                  //  stolenlist[playerVehicle.GetHashCode()] = false;
                drivingstolenvehicle = false;
            }

         //   copveh.SirenActive = false;
            copped.IsPersistent = false;
            Function.Call(Hash.DISABLE_VEHICLE_IMPACT_EXPLOSION_ACTIVATION, copveh, false);
            copveh.LockStatus = VehicleLockStatus.Unlocked;
            copveh.IsPersistent = false;
            copveh.MarkAsNoLongerNeeded();
            copped.MarkAsNoLongerNeeded();


            if (!change)
            {
                offences.Clear();
                state = CopStates.None;
                finalplayerspeed = 0;
                speedingdifference = 0;
                bliplista.Clear();
                wasblipped = false;
                waschanged = false;
            }
            else
                copped = newcop;
            
        }

        bool RunningRedLight()
        {
            if (!redlight)
                return false;

            vehs = World.GetNearbyVehicles(Game.Player.Character.Position, 9);

            if (vehs.Length == 0)
                return false;

            for (int i = 0; i < vehs.Length; i++)
            {
                if (autolista.Count == 0)
                {
                    autolista.Add(vehs[i]);
                    continue;
                }
                if (!autolista.Contains(vehs[i]))
                    autolista.Add(vehs[i]);
            }

            if (autolista.Count == 0)
                return false;

            for (int i = 0; i < autolista.Count; i++)
            {
                if (Vector3.Distance(autolista[i].Position, Game.Player.Character.Position) > 35)
                    autolista.Remove(autolista[i]);
            }

            for (int i = 0; i < autolista.Count; i++)
            {
                if (!Function.Call<bool>(Hash.IS_VEHICLE_STOPPED_AT_TRAFFIC_LIGHTS, autolista[i]))
                    continue;

                float angle = Vector3.Angle(autolista[i].ForwardVector, Game.Player.Character.ForwardVector);

                if (angle > 35)
                    continue;
                else if (angle <= 35 && Vector3.Distance(autolista[i].Position, Game.Player.Character.Position) > 30)
                {
                    Vector3 cardir = autolista[i].Position - Game.Player.Character.Position;
                    // UI.Notify(Vector3.Angle(cardir, Game.Player.Character.ForwardVector) + "");

                    if (Vector3.Angle(cardir, Game.Player.Character.ForwardVector) > 150)
                    {
                        //   UI.ShowSubtitle("RUNNING A RED LIGHT!!", 2000);
                        return true;
                    }

                }

            }
            return false;

        }

        bool DrivingAgainstTraffic(int dat)
        {
            if (!settingagainsttraffic)
                return false;
            if (playerVehicle.Speed < 5)
            {
                againsttrafficticks = 0;
                return false;
            }

            if (dat == 0)
                againsttrafficticks++;

            else
                againsttrafficticks = 0;

            if (againsttrafficticks > 200)
            {
                rikepaikka = World.GetStreetName(Game.Player.Character.Position);
                return true;
            }

            return false;

        }

        bool WithoutHelmet()
        {
            if (!settingwithouthelmet)
                return false;
            if (stoppedforhelmet)
                return false;

            if (playerVehicle.Model.IsBike && playerVehicle.Speed > 3 && !playerVehicle.Model.IsBicycle)
                if (!Game.Player.Character.IsWearingHelmet)
                    return true;
            return false;
        }

        bool Wheelie()
        {
            if (!settingwheelie)
                return false;
            if (playerVehicle.Model.IsBike)
            {
                if (!playerVehicle.IsInAir && !playerVehicle.IsOnAllWheels)
                {
                    wheelieticks++;
                    if (wheelieticks > 150)
                    {
                        //  UI.ShowSubtitle("wheelie", 1000);
                        return true;
                    }
                    return false;
                }
                wheelieticks = 0;
                return false;
            }
            wheelieticks = 0;
            return false;
        }

        bool NotRoadworthy()
        {
            if (!settingnotrw)
                return false;
            if (stoppedforcarfix)
                return false;

            if (playerVehicle.BodyHealth < 700)
                return true;
            // playerVehicle.
            //  if (playerVehicle.LeftHeadLightBroken && playerVehicle.RightHeadLightBroken)
            //      return true;

            return false;
        }

        bool Tailgating()
        {
            if (!settingtailgating)
                return false;
            if (playerVehicle.Speed < 10)
                return false;

            RaycastResult k = World.Raycast(Game.Player.Character.Position + Game.Player.Character.ForwardVector * 3, Game.Player.Character.Position + Game.Player.Character.ForwardVector * 5, IntersectOptions.Everything);
            // RaycastResult k = new RaycastResult();
            // OutputArgument target = new OutputArgument();
            if (k.DitHitEntity)
            {
                Entity tmp = k.HitEntity;
                String tm = tmp.GetType().ToString();

                if (tm.Contains("Vehicle"))
                {
                    tailgatingticks++;
                    Vehicle vehi = (Vehicle)tmp;
                    if (vehi.Equals(playerVehicle))
                        return false;
                    if (vehi.Speed < 10)
                        return false;
                    //  UI.ShowSubtitle(vehi.Speed + " "+tailgatingticks);

                    if (tailgatingticks > 100)
                        return true;


                }
                else
                    tailgatingticks = 0;
                /*  Entity tmp = k.HitEntity;
                  if (tmp.Model.IsVehicle)
                  {
                      Vehicle vehi = (Vehicle)tmp;

                  }*/

            }
            else
                tailgatingticks = 0;

            return false;
        }

        bool Sliding()
        {
            if (!settingdrifting)
                return false;
            if (!playerVehicle.Model.IsCar)
                return false;
            if (prevpos == null || playerVehicle.Speed < 5)
            {
                prevpos = Game.Player.Character.Position;
                return false;
            }

            Vector3 dir = (Game.Player.Character.Position - prevpos).Normalized;
            float angle = Vector3.Angle(dir, Game.Player.Character.ForwardVector);
            if (angle > 25 && angle < 90)
            {
                slidingticks++;
                prevpos = Game.Player.Character.Position;
                if (slidingticks > 7)
                {
                    //  UI.ShowSubtitle("skidding", 500);
                    return true;
                }
                else
                    return false;
            }
            slidingticks = 0;
            return false;
        }

        bool DrivingOnPavement()
        {
            if (!settingonsidewalk)
                return false;
            if (playerVehicle.Speed < 10)
                return false;
            if(Vector3.Distance(World.GetSafeCoordForPed(Game.Player.Character.Position, true), Game.Player.Character.Position) < 0.7f)
            {
               // UI.ShowSubtitle("pavement", 200);
                return true;
            }
                
            return false;
        }

        bool StopSignViolation()
        {
            if (!settingstopsign)
                return false;
            Prop[] props = World.GetNearbyProps(Game.Player.Character.Position, 10);

            if (stopsign == null)
            {
                for (int i = 0; i < props.Length; i++)
                {
                    if (props[i].Model.Hash == -949234773)
                        if (Vector3.Angle(Game.Player.Character.ForwardVector, props[i].ForwardVector) < 15)
                        {
                            if(debugss)
                                UI.Notify("angle = "+Vector3.Angle(Game.Player.Character.ForwardVector, props[i].ForwardVector) + "");
                            stopsign = props[i];
                        }
                }
                return false;
            }
            else
            {
                if (playerVehicle.Speed < stopsignspeed)
                    stopsignspeed = playerVehicle.Speed;
            //    UI.ShowSubtitle(Vector3.Distance(Game.Player.Character.Position, stopsign.Position) + " " +stopsignspeed);
                if (Vector3.Distance(Game.Player.Character.Position, stopsign.Position) > 10)
                {
                    Prop[] props2 = World.GetNearbyProps(Game.Player.Character.Position, 20);
                    for (int i = 0; i < props2.Length; i++)
                    {
                        if (liikennevalot.Contains(props2[i].Model.Hash))
                        {
                            if (debugss)
                                UI.ShowSubtitle("stop sign ignored: traffic lights");
                            stopsign = null;
                            stopsignspeed = 100;
                            return false;
                        }
                    }
                    if (Vector3.Angle(Game.Player.Character.ForwardVector, stopsign.ForwardVector) < 110 && stopsignspeed >= 1.5f)
                    {
                        if (debugss)
                            UI.ShowSubtitle("Ran a stop sign");
                        stopsign = null;
                        stopsignspeed = 100;
                        return true;
                    }
                    else
                    {
                        if (debugss)
                            UI.ShowSubtitle("no violation");
                        stopsign = null;
                        stopsignspeed = 100;
                        return false;
                    }

                }
            }
            return false;
        }

        bool Burnout()
        {
            if (Function.Call<bool>(Hash.IS_VEHICLE_IN_BURNOUT, playerVehicle))
                burnoutticks++;
            else
                
                burnoutticks = 0;
            UI.ShowSubtitle(burnoutticks + " " + playerVehicle.Speed);
            if (burnoutticks > 500)
                return true;
            return false;

            
        }
        void PedAliveTick()
        {
            if (jackeddriver.IsAlive)
            {
                if (!stolenlist.ContainsKey(jackedvehicle.GetHashCode()))
                {
                 //   if (settingnotifystolen)
                 //       UI.Notify("~y~Vehicle theft was reported");
                    stolenlist.Add(jackedvehicle.GetHashCode(), true);
                    drivingstolenvehicle = true;
                }
                    

                if (Game.Player.Character.IsInVehicle() && playerVehicle.Equals(jackedvehicle))
                {
                    // Function.Call(Hash.SET_VEHICLE_IS_STOLEN, jackedvehicle, true);
                    if(settingnotifystolen)
                    UI.Notify("~r~This vehicle has been reported stolen");
                }
            }
            jackeddriver.IsPersistent = false;
            jacking = false;
        }


        void TrackVehicle()
        {
            

            if (Game.Player.Character.IsJacking && settingstolenvehicle && !jacking && !Game.Player.Character.IsInVehicle())
            {
                jackeddriver = Game.Player.Character.GetJackTarget();
                jackedvehicle = jackeddriver.CurrentVehicle;
                if (jackedvehicle == null)
                    return;
                //  hasjacked = true;
                //  jackedticker = ticktimer + 1500;
                if (stolenlist.ContainsKey(jackedvehicle.GetHashCode()))
                    return;
                jacking = true;
                stolenlist.Add(jackedvehicle.GetHashCode(), false);

                Ped[] vehp = jackedvehicle.Passengers;

                if(settingwitnessblips)
                {
                    bliplista.Add(new BlipHandler(jackeddriver, DateTime.Now.AddSeconds(25), false));
                    for (int i = 0; i < vehp.Length; i++)
                    {
                        bliplista.Add(new BlipHandler(vehp[i], DateTime.Now.AddSeconds(25), false));
                    }
                }
                
                for (int i = 0; i < vehp.Length; i++)
                {
                    witnesslista.Add(new WitnessHandler(DateTime.Now.AddSeconds(25), vehp[i], jackedvehicle.GetHashCode()));
                 //   vehp[i].Task.LeaveVehicle(vehp[i].CurrentVehicle, false);
                    vehp[i].Task.ReactAndFlee(Game.Player.Character);
                }
                witnesslista.Add(new WitnessHandler(DateTime.Now.AddSeconds(25), jackeddriver, jackedvehicle.GetHashCode()));
                jacking = false;
                return;
            }

            enterVehicle = Game.Player.Character.GetVehicleIsTryingToEnter();
            if(enterVehicle.Exists())
            {
            /*    UI.Notify(enterVehicle.GetHashCode()+"");
                for (int i = 0; i < haltulista.Count; i++)
                {
                    UI.Notify(haltulista[i].GetHashCode()+"");
                }*/
                int hashi = enterVehicle.GetHashCode();
                for (int i = 0; i < haltulista.Count; i++)
                {
                    if (haltulista[i].GetHashCode() == hashi && recordlista.Count < settingmaxviolations && felonylista.Count == 0 && !licensesuspended)
                    {
                      //  UI.Notify("unpersist");
                        enterVehicle.LockStatus = VehicleLockStatus.Unlocked;
                        enterVehicle.IsPersistent = false;
                        if (Function.Call<bool>(Hash.IS_VEHICLE_ATTACHED_TO_TRAILER, enterVehicle))
                        {
                            OutputArgument trailer = new OutputArgument();
                            Function.Call<Entity>(Hash.GET_VEHICLE_TRAILER_VEHICLE, enterVehicle, trailer);
                            Entity temp = trailer.GetResult<Entity>();

                            if (!temp.Equals(null))
                            {
                              //  UI.Notify("trailer unpersist");
                                temp.IsPersistent = false;
                            }

                        }
                        haltulista.Remove(enterVehicle);
                        break;
                    }
                }
            }
            if (!settingstolenvehicle)
                return;
            if (!enterVehicle.Exists() && !break_in)
                return;
            if (enterVehicle.Model.IsCar && enterVehicle.IsSeatFree(VehicleSeat.Driver) && !break_in)
            {
                if (Function.Call<bool>(Hash.IS_VEHICLE_WINDOW_INTACT, enterVehicle, 0))
                {
                    break_in = true;
                    enterehjadriverside = true;
                }
                if(Function.Call<bool>(Hash.IS_VEHICLE_WINDOW_INTACT, enterVehicle, 1))
                {
                    break_in = true;
                    enterehjapassengerside = true;
                }

            }
            if (!break_in)
                return;
            if (Game.Player.Character.IsInVehicle() && break_in)
            {
                break_in = false;

                if (Function.Call<bool>(Hash.IS_VEHICLE_WINDOW_INTACT, Game.Player.Character.CurrentVehicle, 0) && enterehjadriverside)
                    enterehjadriverside = false;
                if (Function.Call<bool>(Hash.IS_VEHICLE_WINDOW_INTACT, Game.Player.Character.CurrentVehicle, 1) && enterehjapassengerside)
                    enterehjapassengerside = false;

                if (!enterehjapassengerside && !enterehjadriverside)
                    return;
                else
                {
                    
                    enterehjadriverside = false;
                    enterehjapassengerside = false;
                    int radius = 30;
                    if (Game.Player.Character.CurrentVehicle.AlarmActive)
                        radius = 50;
                    Ped[] pedit = World.GetNearbyPeds(Game.Player.Character.Position, radius);

                    for (int i = 0; i < pedit.Length; i++)
                    {
                        if (pedit[i].IsHuman && !pedit[i].Equals(Game.Player.Character) && pedit[i].IsAlive)
                            if (Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, pedit[i].Handle, Game.Player.Character.Handle, 17))
                            {
                                
                                // pedit[i].Task.LookAt(Game.Player.Character, 15000);
                                if(pedit[i].IsOnFoot)
                                    pedit[i].Task.TurnTo(Game.Player.Character, 15000);

                                if(settingwitnessblips)
                                    bliplista.Add(new BlipHandler(pedit[i], DateTime.Now.AddSeconds(15), false));

                                witnesslista.Add(new WitnessHandler(DateTime.Now.AddSeconds(15), pedit[i], Game.Player.Character.CurrentVehicle.GetHashCode()));
                                //   Function.Call(Hash.SET_VEHICLE_IS_STOLEN, Game.Player.Character.CurrentVehicle, true);
                                if (!stolenlist.ContainsKey(Game.Player.Character.CurrentVehicle.GetHashCode()))
                                {
                                    if (settingnotifystolen)
                                        UI.Notify("~y~Vehicle theft was witnessed");
                                    
                                    stolenlist.Add(Game.Player.Character.CurrentVehicle.GetHashCode(), false);  
                                }    
                            }
                    }
                }
            }
            
        }

        void TrackPlayer()
        {
            if (state == CopStates.Ticket || state == CopStates.StepOut)
                return;

            if(scriptenabled)
                TrackVehicle();

            if (scriptenabled && Game.Player.WantedLevel == 0)
            {
                if (!Game.Player.Character.IsInVehicle())
                    return;

                if (Function.Call<bool>(Hash.IS_VEHICLE_SIREN_ON, playerVehicle))
                    return;

                if (!World.GetStreetName(Game.Player.Character.Position).Equals(currentstreet))
                {
                    currentstreet = World.GetStreetName(Game.Player.Character.Position);

                    OutputArgument streetname = new OutputArgument();
                    OutputArgument crossingroad = new OutputArgument();

                    Function.Call<bool>(Hash.GET_STREET_NAME_AT_COORD, Game.Player.Character.Position.X, Game.Player.Character.Position.Y, Game.Player.Character.Position.Z, streetname, crossingroad);
                    string crossing = Function.Call<string>(Hash.GET_STREET_NAME_FROM_HASH_KEY, crossingroad.GetResult<int>());

                    if (freeways.Contains(currentstreet) || freeways.Contains(crossing))
                        speedlimit = hwaylimit / 2.236936f;
                    else
                        speedlimit = generallimit / 2.236936f;

                }
                if (!Game.Player.Character.CurrentVehicle.Equals(playerVehicle))
                {
                    playerVehicle = Game.Player.Character.CurrentVehicle;
                    Wait(1);
                    stoppedforcarfix = false;
                    stoppedforhelmet = false;
                    
                    if(settingstolenvehicle)
                    {
                        if (stolenlist.ContainsKey(playerVehicle.GetHashCode()))
                        {
                            if (stolenlist[playerVehicle.GetHashCode()])
                            {
                                  if (settingnotifystolen)
                                    UI.Notify("~r~This vehicle has been reported stolen");
                                drivingstolenvehicle = true;
                            }
                            else
                                drivingstolenvehicle = false;
                        }
                        else
                            drivingstolenvehicle = false;
                    }
                    
                    //       if (Function.Call<bool>(Hash.IS_VEHICLE_STOLEN, playerVehicle))
                    //           UI.Notify("~r~This vehicle has been reported stolen");
                    
                }
                
                if (playerVehicle.IsInAir)
                    return;

                if (!Function.Call<bool>(Hash._IS_VEHICLE_ENGINE_ON, playerVehicle))
                    return;

                if (drivingstolenvehicle)
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(30, true, false, true))
                        {
                            AddOffence("stolen");
                            SetChase();
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if (ChaserCanSee())
                            AddOffence("stolen");
                    }
                }

                if (DrivingAgainstTraffic(Function.Call<int>(Hash.GET_TIME_SINCE_PLAYER_DROVE_AGAINST_TRAFFIC, Game.Player)))
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(range, true, false, false))
                        {
                            AddOffence("againsttraffic");
                            atrikepaikka = World.GetStreetName(Game.Player.Character.Position);
                            SetChase();
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if (ChaserCanSee())
                        {
                            AddOffence("againsttraffic");
                            atrikepaikka = World.GetStreetName(Game.Player.Character.Position);
                        }     
                    }
                }

                if (NotRoadworthy())
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(range, true, false, true))
                        {
                            AddOffence("notrw");
                            SetChase(); 
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if (ChaserCanSee())
                            AddOffence("notrw");
                    }
                }
                float sp1 = playerVehicle.Speed;
                Wait(1);
                float sp2 = playerVehicle.Speed;
                // if (playerVehicle.Speed+2 < playerspeedthen)
                //   UI.Notify(playerVehicle.Speed - playerspeedthen + "");
                if (sp2 + 5 < sp1)
                {
                   // UI.ShowSubtitle(Math.Round((sp2 - sp1) * 100) / 100 + " ");
                    if (settingcolliding)
                    {
                        if (state == CopStates.None)
                        {
                            if (GetCopInVehicle(range, true, false, false))
                            {
                                AddOffence("collision");
                                SetChase();
                            }
                        }
                        else if (state == CopStates.Chase || state == CopStates.Follow)
                        {
                            if (ChaserCanSee())
                                AddOffence("collision");
                        }
                    }
                }
                    
                if (Function.Call<bool>(Hash.IS_PED_RUNNING_MOBILE_PHONE_TASK, Game.Player.Character))
                {
                    if(settingmobilephone)
                    {
                        if (state == CopStates.None)
                        {
                            if (GetCopInVehicle(range, true, false, true))
                            {
                                AddOffence("phone");
                                SetChase();
                            }
                        }
                        else if (state == CopStates.Chase || state == CopStates.Follow)
                        {
                            if (ChaserCanSee())
                                AddOffence("phone");
                        }
                    }
                }
                if (Tailgating())
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(range, true, false, true))
                        {
                            AddOffence("tailgating");
                            SetChase(); 
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if (ChaserCanSee())
                            AddOffence("tailgating");
                    }
                }
                if (Sliding())
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(range, true, false, false))
                        {
                            AddOffence("sliding");
                            SetChase(); 
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if (ChaserCanSee())
                            AddOffence("sliding");
                    }
                }
                if (RunningRedLight())
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(range, true, false, false))
                        {
                            AddOffence("redlight");
                            SetChase();  
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if (ChaserCanSee())
                            AddOffence("redlight");
                    }
                }
                if (WithoutHelmet())
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(range, true, false, true))
                        {
                            AddOffence("helmet");
                            SetChase();
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if (ChaserCanSee())
                            AddOffence("helmet");
                    }
                }
                if (Wheelie())
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(range, true, false, false))
                        {
                            AddOffence("wheelie");
                            SetChase();
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if(ChaserCanSee())
                            AddOffence("wheelie");
                    }
                }

                if(DrivingOnPavement())
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(range, true, false, false))
                        {
                            AddOffence("onpavement");
                            SetChase();
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if(ChaserCanSee())
                            AddOffence("onpavement");
                    }                  
                }

                if (StopSignViolation())
                {
                    if (state == CopStates.None)
                    {
                        if (GetCopInVehicle(range, true, false, true))
                        {
                            AddOffence("stopsign");
                            SetChase();
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        if (ChaserCanSee())
                            AddOffence("stopsign");
                    }
                }
                /*   if (Burnout())
                   {
                       if (state == CopStates.None)
                       {
                           if (GetCopInVehicle(30, true, false, false))
                           {
                               AddOffence("burnout");
                               SetChase();
                           }
                       }
                       else if (state == CopStates.Chase || state == CopStates.Follow)
                       {
                           if (ChaserCanSee())
                               AddOffence("burnout");
                       }
                   }*/

                //  playerspeed = playerVehicle.Speed;
               // UI.ShowSubtitle(/*(int)(speedingdifference * 2.236936f) + " " + rikepaikka + " \n" + (int)(finalplayerspeed * 2.236936f) + "/" + (int)(speedlimitatcaught * 2.236936f) + */" "+(int)(speedlimit * 2.236936f));
                if (playerVehicle.Speed > (speedlimit + 4) /*&& playerVehicle.Speed > finalplayerspeed*/)
                {
                    
                    if (state == CopStates.None)
                    {
                        if(GetCopInVehicle(range, true, false, false))
                        {
                            AddOffence("speeding");
                            speedingdifference = playerVehicle.Speed - speedlimit;
                            rikepaikka = World.GetStreetName(Game.Player.Character.Position);
                            speedlimitatcaught = speedlimit;
                            finalplayerspeed = playerVehicle.Speed;
                            SetChase();   
                        }
                    }
                    else if (state == CopStates.Chase || state == CopStates.Follow)
                    {
                        AddOffence("speeding");
                        if ((playerVehicle.Speed - speedlimit) > speedingdifference && ChaserCanSee())
                        {
                            rikepaikka = World.GetStreetName(Game.Player.Character.Position);
                            finalplayerspeed = playerVehicle.Speed;
                            speedingdifference = playerVehicle.Speed - speedlimit;
                            speedlimitatcaught = speedlimit;
                        }      
                    }
                }
            }
        }

        int SpeedLimitAtStreet()
        {
            if (freeways.Contains(rikepaikka))
                return hwaylimit;
            return generallimit;
        }

        void Clearplayer()
        {
            offences.Clear();
        }

        void LisaaRecordiin(int lkm)
        {
            for (int i = 0; i < lkm; i++)
            {
                recordlista.Add(new Record(DateTime.Now.AddMinutes(settingexpireminutes)));
            }
        }

        void AddOffence(string offence)
        {
            
            if (!offences.Contains(offence))
            {
                offences.Add(offence);
                if(debugv)
                    UI.Notify(offence);
            }
                        
        }

        List<string> ReturnOffences()
        {
            return offences;
        }

        void RecordLoop()
        {
            for (int i = 0; i < recordlista.Count; i++)
            {
                recordlista.Remove(recordlista[i]);
            }
        }

        bool OdotusLooppi(int secs)
        {

            for (int i = 0; i < secs; i++)
            {
                if (!IsInterrupted())
                    Wait(1000);
                else
                    return true;
            }

            return false;
        }

        void MoreCops()
        {
            Ped[] copsi = World.GetNearbyPeds(Game.Player.Character.Position, 150);

            for (int i = 0; i < copsi.Length; i++)
            {
                if (Function.Call<int>(Hash.GET_PED_TYPE, copsi[i]) == 6 || addonpeds.Contains(copsi[i].Model.Hash))
                {
                    if (backuppedit.Contains(copsi[i]))
                        continue;
                    if (!copsi[i].Equals(Game.Player.Character))
                    {
                        backuplista.Add(new CopsSentHandler(copsi[i]));
                        backuppedit.Add(copsi[i]);
                        if (copsi[i].IsInVehicle())
                        {
                            Vehicle tempveh = copsi[i].CurrentVehicle;
                            if (tempveh.Model.IsCar || tempveh.Model.IsBike)
                            {
                                tempveh.SirenActive = true;
                            }
                        }
                    }
                }
            }
        }

        void SendCops()
        {
            eventstate = EventState.None;
            waschanged = false;
            
            fleevehicle = playerVehicle;
            fleevehicle.IsPersistent = true;
            // DateTime endtime = DateTime.Now.AddSeconds(60);
            bool vehiclefound = false;
            int times = 0;
            while(Game.Player.WantedLevel == 0 && !Game.Player.Character.IsDead)
            {
                if (Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, playerVehicle.Handle, copped.Handle, 17) || times > 120)
                {
                    if(Game.Player.Character.IsInVehicle())
                    {
                        if (Game.Player.Character.CurrentVehicle.Equals(fleevehicle))
                        {
                            waschanged = true;
                            break;
                        }

                    }
                  //  UI.Notify("~y~Your vehicle was found");
                    vehiclefound = true;
                    break;
                }
                Wait(500);
                times++;
            }
            if (waschanged)
                Action();
            if (!vehiclefound)
            {
                EndEvent(false);
                return;
            }

            Function.Call(Hash.DISABLE_VEHICLE_IMPACT_EXPLOSION_ACTIVATION, copveh, false);

            if (blips && copblip.Exists())
                copblip.Remove();
            backuplista.Add(new CopsSentHandler(copped));
            Ped[] copsi = World.GetNearbyPeds(playerVehicle.Position, 500);

            times = 0;
            

            while (times < 120)
            {
                if (Game.Player.WantedLevel > 0 || Game.Player.Character.IsDead/* || DateTime.Now > endtime*/)
                    break;

                for (int i = 0; i < backuplista.Count; i++)
                {
                    backuplista[i].Check(false);
                }

                MoreCops();
                Wait(500);
                times++;
            }

            for (int i = 0; i < backuplista.Count; i++)
            {
                backuplista[i].ped.Task.ClearAll();
            }

            for (int i = 0; i < bliplista.Count; i++)
            {
                bliplista[i].Raukea();
            }

            fleevehicle.IsPersistent = false;
            fleevehicle = null;
            backuplista.Clear();
            backuppedit.Clear();
            EndEvent(false);
         //   UI.Notify("ended");
        }
    }

    class Record
    {
        DateTime old;
   //     int expireminutes;

        public Record(DateTime old)
        {
            this.old = old;
       //     this.expireminutes = expireminutes;
        }

        public bool Rauennut()
        {
            int arresttime = Function.Call<int>(Hash.GET_TIME_SINCE_LAST_ARREST);
            int deathtime = Function.Call<int>(Hash.GET_TIME_SINCE_LAST_DEATH);

            if ((arresttime > 1000 && arresttime < 30000) || (deathtime > 1000 && deathtime < 30000) || (old < DateTime.Now))
                return true;
            else
                return false;
        }
    }

    class WitnessHandler
    {
        DateTime old { get; set; }
        public Ped ped { get; }
        public int autohash { get; }

        public WitnessHandler(DateTime old, Ped ped, int autohash)
        {
            this.old = old;
            this.ped = ped;
            ped.IsPersistent = true;
            this.autohash = autohash;
        }

        public bool Rauennut()
        {
            if ((old < DateTime.Now))
                return true;   
            else
                return false;
        }
    }

    class CopsSentHandler : PullMeOverMain
    {
        public Ped ped { get; set; }
        Vector3 nextpos;

        public CopsSentHandler(Ped ped)
        {
            bliplista.Add(new BlipHandler(ped, DateTime.Now.AddSeconds(60), true));
            this.ped = ped;
            
         //   ped.IsPersistent = true;
            SetPos();
        }

        void SetPos()
        {
            if (ped.IsInVehicle())
            {
                if(ped.CurrentVehicle.GetPedOnSeat(VehicleSeat.Driver).Equals(ped))
                {
                    if (playerVehicle != null)
                        nextpos = playerVehicle.Position;
                    else    
                        nextpos = World.GetNextPositionOnStreet(Game.Player.Character.Position);
                    ped.Task.DriveTo(ped.CurrentVehicle, nextpos, 10, 50);
                }    
            }
            else
            {
                nextpos = World.GetSafeCoordForPed(Game.Player.Character.Position, false);
                ped.Task.RunTo(nextpos);
            }
            float dist2 = Vector3.Distance(Game.Player.Character.Position, nextpos);
        }
        public void Check(bool end)
        {
            if (Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, ped.Handle, Game.Player.Character.Handle, 17) && Vector3.Distance(ped.Position, Game.Player.Character.Position) < range)
            {
                if(Game.Player.Character.IsInVehicle())
                {
                    if(Game.Player.Character.CurrentVehicle.Equals(fleevehicle))
                        Game.Player.WantedLevel = 1;
                    else if(Vector3.Distance(ped.Position, Game.Player.Character.Position) < 10)
                        Game.Player.WantedLevel = 1;
                }
                else
                    Game.Player.WantedLevel = 1;
            }
                

            if (Game.Player.WantedLevel == 0 && !end)
            {
                float dist = Vector3.Distance(ped.Position, nextpos);
                if (ped.IsInVehicle())
                {
                    if(dist < 15 )
                    {
                       // UI.Notify("liivi");
                        ped.Task.LeaveVehicle(ped.CurrentVehicle, false);
                        nextpos = World.GetSafeCoordForPed(Game.Player.Character.Position, false);
                        ped.Task.RunTo(nextpos);
                    }
                }
                else
                {
                    if(dist < 10)
                    {
                      //  UI.Notify("alle 5");
                        nextpos = World.GetSafeCoordForPed(Game.Player.Character.Position, false);
                        ped.Task.RunTo(nextpos);
                    }
                }
            }
            else
                ped.Task.ClearAll();
        }
    }

    public class BlipHandler
    {
        Blip cblip;
        Ped cped;
        DateTime ctimer;


        public BlipHandler(/*Blip blip, */Ped ped, DateTime time, bool iscop)
        {
            cped = ped;
            ctimer = time;

            if(!ped.CurrentBlip.Exists())
            {
                cped.AddBlip();
                cblip = ped.CurrentBlip;

                if (iscop)
                    cblip.Color = BlipColor.Blue;
                else
                    cblip.Color = BlipColor.Yellow;

                    cblip.Scale = 0.5f;
            }
        }

        public bool Rauennut()
        {
            if (ctimer < DateTime.Now)
            {
                if (cped.Exists())
                    if (cped.CurrentBlip.Exists())
                        cped.CurrentBlip.Remove();
                return true;
            }
            return false;
        }

        public void Raukea()
        {
            if(cped.Exists())
                if(cped.CurrentBlip.Exists())
                    cped.CurrentBlip.Remove();
        }
    }
}

