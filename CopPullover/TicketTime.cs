﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;

namespace PullMeOver
{
    class TicketTime : PullMeOverMain
    {

        public TicketTime()
        {
        }

        Vector3 StoppedPlayerPostion()
        {
            if (playerVehicle.Model.IsCar)
            {
                if (playerVehicle.IsDoorBroken(VehicleDoor.FrontLeftDoor))
                    return PlayaPosition();
                else if (playerVehicle.HasBone("seat_dside_f") && playerVehicle.HasBone("seat_pside_f"))
                {
                    Vector3 vector = playerVehicle.GetBoneCoord("seat_dside_f") - playerVehicle.GetBoneCoord("seat_pside_f");
                    return playerVehicle.GetBoneCoord("seat_dside_f") + vector;
                }

                else
                    return PlayaPosition();
            }
            else if (playerVehicle.Model.IsBicycle || playerVehicle.Model.IsBike)
            {
                Vector3 pos4 = Vector3.Cross(Game.Player.Character.ForwardVector, -Game.Player.Character.UpVector);
                return playerVehicle.Position + pos4;
            }
            else
                return PlayaPosition();
        }

        Vector3 PlayaPosition()
        {
            Vector3 pposi;
            Vehicle vehi = playerVehicle;
            Game.Player.Character.Task.WarpOutOfVehicle(vehi);
            Wait(0);
            pposi = Game.Player.Character.Position;
            Game.Player.Character.Task.WarpIntoVehicle(vehi, VehicleSeat.Driver);
            return pposi;
        }

        void Arrest()
        {
            finalstop = Game.Player.Character.Position;
            if (OdotusLooppi(2))
                return;
            copped.Task.LeaveVehicle(copveh, false);

            copped.Task.ClearAll();
            copped.Weapons.Give(WeaponHash.StunGun, 1, false, true);
            copped.Weapons.Give(WeaponHash.Pistol, 120, true, true);
            Function.Call(Hash.REMOVE_PED_FROM_GROUP, copped.Handle);

            copped.RelationshipGroup = World.AddRelationshipGroup("Criminal");
            World.SetRelationshipBetweenGroups(Relationship.Hate, copped.RelationshipGroup, Game.Player.Character.RelationshipGroup);
            World.SetRelationshipBetweenGroups(Relationship.Hate, Game.Player.Character.RelationshipGroup, copped.RelationshipGroup);
            //   Wait(1);
            if (OdotusLooppi(1))
                return;
            copped.Task.AimAt(Game.Player.Character, -1);
            Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "CHALLENGE_THREATEN", "SPEECH_PARAMS_MEGAPHONE");



            UI.ShowSubtitle("~o~Cop: Get out of the vehicle!", 8000);
            if (OdotusLooppi(10))
                return;

            Vector3 playpos = StoppedPlayerPostion();
            Function.Call(Hash.TASK_GOTO_ENTITY_AIMING, copped, Game.Player.Character.Handle, 1.5f, 30f);
            //    copped.Task.GoTo(playpos, false, -1);
            int ticks = 0;
            while (!IsInterrupted())
            {
                //Function.Call(Hash.DRAW_LINE, copped.Position.X, copped.Position.Y, copped.Position.Z, playpos.X, playpos.Y, playpos.Z, 255, 0, 0, 255);
                float dist = Vector3.Distance(playpos, copped.Position);
                float dist2 = Vector3.Distance(Game.Player.Character.Position, finalstop);
                ticks++;

                if (dist2 > 5 || !Game.Player.Character.IsInVehicle())
                {
                    copped.Task.ClearAll();
                    Game.Player.WantedLevel = 1;
                    break;
                }

                if (dist < 1f)
                {
                    break;
                }
                else if (dist < 2.5f && ticks > 1500)
                    break;

                if (ticks > 3000)
                    break;
                // UI.ShowSubtitle(ticks + " " + dist, 500);
                Wait(0);
            }
            copped.Task.Arrest(Game.Player.Character);
            Wait(2000);
            Game.Player.WantedLevel = 1;
            if (OdotusLooppi(10))
                return;
        }

        public void Ticketer()
        {
            if (OdotusLooppi(5))
                return;
            state = CopStates.Ticket;
            if (offences.Contains("stolen"))
            {
                Arrest();
                return;
            }
            copped.Task.LeaveVehicle(copveh, true);
            /*       if(!copveh.IsSeatFree(VehicleSeat.Passenger))
                   {
                       partner = true;
                       coppartner = copveh.GetPedOnSeat(VehicleSeat.Passenger);
                       coppartner.Task.LeaveVehicle(copveh, false);
                       coppartner.BlockPermanentEvents = true;

                   }*/
            finalstop = Game.Player.Character.Position;
            if (OdotusLooppi(1))
                return;

            //     if(partner)
            //         coppartner.Task.StandStill(-1);
            float dist = Vector3.Distance(Game.Player.Character.Position, copped.Position);
            float dist2;
            Vector3 playpos = StoppedPlayerPostion();



            Wait(1);
            copped.Task.GoTo(playpos, false, -1);
            int ticks = 0;
            while (!IsInterrupted())
            {
                //Function.Call(Hash.DRAW_LINE, copped.Position.X, copped.Position.Y, copped.Position.Z, playpos.X, playpos.Y, playpos.Z, 255, 0, 0, 255);
                dist = Vector3.Distance(playpos, copped.Position);
                dist2 = Vector3.Distance(Game.Player.Character.Position, finalstop);
                ticks++;

                if (dist2 > 5 || !Game.Player.Character.IsInVehicle())
                {
                    copped.Task.ClearAll();
                    Game.Player.WantedLevel = 1;
                    break;
                }

                if (dist < 1f)
                {
                    break;
                }
                else if (dist < 2.5f && ticks > 1500)
                    break;

                if (ticks > 3000)
                    break;
                // UI.ShowSubtitle(ticks + " " + dist, 500);
                Wait(0);
            }

            if (IsInterrupted()) return;
            if (ticks > 3000)
            {
                EndEvent();

                UI.Notify("~r~Pullover aborted");
                return;
            }

            copped.Task.TurnTo(Game.Player.Character, 1500);
            Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_HI", "SPEECH_PARAMS_FORCE");
            while (Function.Call<bool>(Hash.IS_ANY_SPEECH_PLAYING, copped))
            {
                Wait(0);
            }

            if (OdotusLooppi(2))
                return;
            bool arrest = false;

            if (!arrest)
            {
                Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "KIFFLOM_GREET", "SPEECH_PARAMS_FORCE");
                //     Function.Call(Hash.TASK_START_SCENARIO_IN_PLACE, copped, "CODE_HUMAN_MEDIC_TIME_OF_DEATH", 0, 1);
                Function.Call(Hash.TASK_START_SCENARIO_AT_POSITION, copped, "CODE_HUMAN_MEDIC_TIME_OF_DEATH", copped.Position.X,
                    copped.Position.Y, copped.Position.Z, copped.Heading, offences.Count - 1 * 8000, false, false);

                if (OdotusLooppi(2))
                    return;

                for (int i = 0; i < offences.Count; i++)
                {
                    if (offences[i].Equals("speeding"))
                    {
                        UI.ShowSubtitle("Cop: I clocked you doing ~r~" + (int)(finalplayerspeed * 2.236936f) + " ~w~in a " + (int)(speedlimit * 2.236936f) +
                        " MPH zone on ~g~" + rikepaikka + "~w~!", 5000);
                        //   Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("onpavement"))
                    {
                        UI.ShowSubtitle("Cop: I saw you driving recklessly on pavement at ~g~" + rikepaikka + "~w~!", 8000);
                        //    Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("againsttraffic"))
                    {
                        UI.ShowSubtitle("Cop: You were driving against traffic on ~g~" + rikepaikka + "~w~!", 8000);
                        //   Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("collision"))
                    {
                        UI.ShowSubtitle("Cop: That was quite a collision you had there.", 8000);
                        //   Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("notrw"))
                    {
                        UI.ShowSubtitle("Cop: Your vehicle is heavily damaged. Please get it fixed.", 8000);
                        //   Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("phone"))
                    {
                        UI.ShowSubtitle("Cop: What was that in your hands? A phone? You know that's a traffic violation", 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("tailgating"))
                    {
                        UI.ShowSubtitle("Cop: You were driving really close to other vehicle", 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("sliding"))
                    {
                        UI.ShowSubtitle("Cop: You seem to have trouble controlling your vehicle", 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("redlight"))
                    {
                        UI.ShowSubtitle("Cop: You ran a red light!", 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("helmet"))
                    {
                        if (Game.Player.Character.IsWearingHelmet)
                            UI.ShowSubtitle("Cop: Oh, now you found your helmet", 8000);
                        else
                            UI.ShowSubtitle("Cop: Where is your helmet?!", 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }
                    else if (offences[i].Equals("wheelie"))
                    {
                        UI.ShowSubtitle("Cop: Public road is not a stunt track!", 8000);
                        //  Wait(1500);
                        Spiikki("ticket");
                    }

                    if (OdotusLooppi(6))
                        break;

                    if (i != offences.Count - 1)
                    {

                        if (IsInterrupted())
                            break;
                        UI.ShowSubtitle("Cop: Also...", 8000);
                        if (OdotusLooppi(4))
                            break;
                    }
                }
            }

            ticks = 0;

            if (Function.Call<bool>(Hash.IS_PED_USING_ANY_SCENARIO, copped))
            {
                copped.Task.ClearAll();
                if (OdotusLooppi(7))
                    return;
            }

            if (IsInterrupted()) { EndEvent(); return; }

            Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "CRIMINAL_WARNING", "SPEECH_PARAMS_FORCE");
            Wait(200);
            if (!Function.Call<bool>(Hash.IS_ANY_SPEECH_PLAYING, copped))
                Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_BYE", "SPEECH_PARAMS_FORCE");

            for (int i = 0; i < offences.Count; i++)
            {
                if (offences[i].Equals("speeding"))
                {
                    int limit = (int)(speedlimit * 2.236936f);
                    int pspeed = (int)(finalplayerspeed * 2.236936f);

                    UI.Notify("You were fined ~r~$" + (pspeed - limit) * 20 + "~w~ for going " + (pspeed - limit) + " over the speed limit.");
                    Game.Player.Money = Game.Player.Money - (pspeed - limit) * 20;
                }
                else if (offences[i].Equals("onpavement"))
                {
                    UI.Notify("You were fined ~r~$350 ~w~for reckless driving.");
                    Game.Player.Money = Game.Player.Money - 350;
                }
                else if (offences[i].Equals("againsttraffic"))
                {
                    UI.Notify("You were fined ~r~$250 ~w~for careless driving.");
                    Game.Player.Money = Game.Player.Money - 250;
                }
                else if (offences[i].Equals("collision"))
                {
                    UI.Notify("You were fined ~r~$500 ~w~for damage to property.");
                    Game.Player.Money = Game.Player.Money - 500;
                }
                else if (offences[i].Equals("notrw"))
                {
                    UI.Notify("You were fined ~r~$180 ~w~for unroadworthy vehicle.");
                    Game.Player.Money = Game.Player.Money - 180;
                    stoppedforcarfix = true;
                }
                else if (offences[i].Equals("phone"))
                {
                    UI.Notify("You were fined ~r~$100 ~w~for using a mobile phone while driving.");
                    Game.Player.Money = Game.Player.Money - 100;
                }
                else if (offences[i].Equals("tailgating"))
                {
                    UI.Notify("You were fined ~r~$100 ~w~for tailgating.");
                    Game.Player.Money = Game.Player.Money - 100;
                }
                else if (offences[i].Equals("sliding"))
                {
                    UI.Notify("You were fined ~r~$350 ~w~for reckless driving.");
                    Game.Player.Money = Game.Player.Money - 350;
                }
                else if (offences[i].Equals("redlight"))
                {
                    UI.Notify("You were fined ~r~$450 ~w~for running a red light.");
                    Game.Player.Money = Game.Player.Money - 450;
                }
                else if (offences[i].Equals("helmet"))
                {
                    UI.Notify("You were fined ~r~$200 ~w~for not wearing motorcycle helmet.");
                    Game.Player.Money = Game.Player.Money - 200;
                    stoppedforhelmet = true;
                }
                else if (offences[i].Equals("wheelie"))
                {
                    UI.Notify("You were fined ~r~$350 ~w~for reckless driving.");
                    Game.Player.Money = Game.Player.Money - 350;
                }
            }

            copped.Task.EnterVehicle(copveh, VehicleSeat.Driver);
            copped.BlockPermanentEvents = false;
            Wait(1000);
            EndEvent();

        }

        void Spiikki(string keissi)
        {
            string randi = "";

            while (!Function.Call<bool>(Hash.IS_ANY_SPEECH_PLAYING, copped))
            {
                randi = ticketyells[rand.Next(ticketyells.Length)];
                Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, randi, "SPEECH_PARAMS_FORCE");
                Wait(0);
            }
            UI.Notify(randi);
        }
    }
}

/* if (arrest)
             {
                 UI.ShowSubtitle("Cop: That was quite fast was you were going back there...", 4000);

                 copped.Task.GoTo(copveh.Position - playpos + playpos, false, -1);
                 Wait(1000);
                 copped.Task.TurnTo(Game.Player.Character, 1500);
                 Wait(1500);
                 Function.Call(Hash.TASK_START_SCENARIO_IN_PLACE, copped, "WORLD_HUMAN_COP_IDLES", 0, 1);
                 Function.Call(Hash._PLAY_AMBIENT_SPEECH2, copped, "GENERIC_INSULT_HIGH", "SPEECH_PARAMS_FORCE");
                 UI.ShowSubtitle("Cop: Please step out of the vehicle", 6000);

                 while (!IsInterrupted())
                 {
                     dist2 = Vector3.Distance(Game.Player.Character.Position, finalstop);
                     if (dist2 > 5)
                     {
                         copped.Task.ClearAll();
                         Game.Player.WantedLevel = 2;
                         break;
                     }
                     else if (!Game.Player.Character.IsInVehicle())
                     {
                         copped.Task.StandStill(2000);
                         Game.Player.WantedLevel = 1;
                         copped.Task.Arrest(Game.Player.Character);
                         break; 
                     }
                     Wait(0);
                 }
                 EndEvent();
             }*/
