﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;

namespace PullMeOver
{
    public partial class PullMeOverMain : Script
    {
        List<Ped> potentialcops = new List<Ped>();
        internal static void FindCops()
        {
            //UI.ShowSubtitle("copfind", 500);
            for (int i = 0; i < coplista.Count; i++)
            {
                if (!coplista[i].Exists())
                    coplista.Remove(coplista[i]);
            }
        

            copsi = World.GetNearbyPeds(Game.Player.Character.Position, 100);

            for (int i = 0; i < copsi.Length; i++)
            {
                if (coplista.Contains(copsi[i]))
                    continue;
                if (Function.Call<int>(Hash.GET_PED_TYPE, copsi[i]) == 6 || addonpeds.Contains(copsi[i].Model.Hash) && !copsi[i].Equals(Game.Player.Character))
                    coplista.Add(copsi[i]); 
            }  
        }

        Ped GetClosest(bool setchase, bool changechaser)
        {
            float closest = 500;
            float distance = 0;
            Ped cop = null;
            for (int i = 0; i < potentialcops.Count; i++)
            {
                distance = Vector3.Distance(potentialcops[i].Position, Game.Player.Character.Position);
                
                if (distance < closest)
                {
                    if (setchase || changechaser)
                    {
                        if (!potentialcops[i].IsInVehicle())
                            continue;
                        if (!potentialcops[i].CurrentVehicle.GetPedOnSeat(VehicleSeat.Driver).Equals(potentialcops[i]))
                            continue;
                        if (setchase && !changechaser)
                            copped = potentialcops[i];
                        if (changechaser)
                            newcop = potentialcops[i];
                    }
                    closest = distance;
                    cop = potentialcops[i];
                }
            }
            return cop;
        }
        
        Ped GetCop(int rangex, bool cansee, bool driver, bool pedcop, bool setchase, bool changechaser)
        {
            bool firstloop = false;
            potentialcops.Clear();
            for (int i = 0; i < coplista.Count; i++)
            {
                firstloop = true;
                float dist = Vector3.Distance(coplista[i].Position, Game.Player.Character.Position);

                if (driver)
                {
                    if (!coplista[i].IsInVehicle())
                        continue;
                    if (!coplista[i].CurrentVehicle.GetPedOnSeat(VehicleSeat.Driver).Equals(coplista[i]))
                        continue;
                    if (coplista[i].CurrentVehicle.SirenActive)
                        continue;
                }
                if (cansee)
                {
                    if (!Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, coplista[i].Handle, Game.Player.Character.Handle, 17) || dist > rangex)
                        continue;
                }

                potentialcops.Add(coplista[i]);
            }

            if (firstloop && potentialcops.Count != 0)
                return GetClosest(setchase, changechaser);
            
            if (pedcop && settingcanbereported)
            {
                Ped witness = GetCop(40, true, false, false, false, false);
                if (witness != null)
                {
                    if (witness.IsOnFoot)
                        witness.Task.TurnTo(Game.Player.Character, 6000);

                    Function.Call(Hash._PLAY_AMBIENT_SPEECH2, witness, "SUSPECT_SPOTTED", "SPEECH_PARAMS_STANDARD");
                    bliplista.Add(new BlipHandler(witness, DateTime.Now.AddSeconds(6), true));
                    return GetCop(400, false, true, false, true, false);
                }
            }
            return null;
        }
    }
}
