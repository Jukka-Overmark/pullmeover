﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTA;
using GTA.Native;
using GTA.Math;

namespace PullMeOver
{
    class PullThemOver : Script
    {
        int nyt = DateTime.Now.Second;

      //  List<Vehicle> autolista = new List<Vehicle>();
        List<CopHandler> coplista = new List<CopHandler>();
        List<PedHandler> pedlista = new List<PedHandler>();

        public PullThemOver()
        {
            Tick += OnTick;
        }

        void OnTick(object sender, EventArgs e)
        {
            TimeTicker();
            CopTasker();
        }

        void TimeTicker()
        {
            if(nyt != DateTime.Now.Second)
            {
                nyt = DateTime.Now.Second;
                ListClearer();
                Vehicle[] vehs = World.GetNearbyVehicles(Game.Player.Character.Position, 100);
                for (int i = 0; i < vehs.Length; i++)
                {
                    if(!vehs[i].IsSeatFree(VehicleSeat.Driver))
                    {
                        Ped driver = vehs[i].GetPedOnSeat(VehicleSeat.Driver);
                        for (int y = 0; y < pedlista.Count; y++)
                        {
                            if (pedlista[y].getPed().Equals(driver))
                                continue;
                        }
                        if (!driver.Equals(Game.Player.Character) && Function.Call<int>(Hash.GET_PED_TYPE, driver) != 6)
                        {
                            pedlista.Add(new PedHandler(driver));
                        }    

                        else if (Function.Call<int>(Hash.GET_PED_TYPE, driver) == 6)
                            coplista.Add(new CopHandler(driver));
                    }
                }

                for (int i = 0; i < pedlista.Count; i++)
                {
                    if(pedlista[i].Violating())
                    {
                        for (int y = 0; y < coplista.Count; y++)
                        {
                            if (coplista[y].IsBusy())
                                continue;
                            if(Function.Call<bool>(Hash.HAS_ENTITY_CLEAR_LOS_TO_ENTITY, coplista[y].getPed().Handle, pedlista[i].getPed().Handle, 17))
                            {
                                coplista[y].SetSuspect(pedlista[i].getPed());
                                Vehicle pedveh = pedlista[i].getPed().CurrentVehicle;
                                Vehicle copveh = coplista[y].getPed().CurrentVehicle;
                                UI.Notify("chase");
                                Vector3 coord = World.GetSafeCoordForPed(pedlista[i].getPed().Position, false);
                                coplista[y].SetBusy(true);
                                copveh.SirenActive = true;
                                Function.Call(Hash.DISABLE_VEHICLE_IMPACT_EXPLOSION_ACTIVATION, copveh, true);
                                Function.Call(Hash.SET_DRIVE_TASK_CRUISE_SPEED, pedlista[i].getPed(), 0f);
                              //  pedlista[i].getPed().Task.DriveTo(pedveh, coord, 2, 10);
                                coplista[y].getPed().Task.DriveTo(copveh, coord + pedveh.ForwardVector * -5, 2, 10);

                       //         Function.Call(Hash.TASK_VEHICLE_CHASE, coplista[y].getPed(), pedlista[i].getPed().Handle);
                                //       Function.Call(Hash.SET_TASK_VEHICLE_CHASE_IDEAL_PURSUIT_DISTANCE, copped, 50f);
                                //       Function.Call(Hash.SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG, copped, 1, true);
                                Function.Call(Hash.SET_DRIVER_ABILITY, coplista[y].getPed(), 0.9f);
                      //          Function.Call(Hash.SET_TASK_VEHICLE_CHASE_IDEAL_PURSUIT_DISTANCE, coplista[y].getPed(), 20);
                      //          Function.Call(Hash.SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG, coplista[y].getPed(), 32, true);

                               // coplista[y].getPed().Weapons.RemoveAll();
                            }
                        }
                    }

                }
            }
        }

        void CopTasker()
        {
            for (int i = 0; i < coplista.Count; i++)
            {
                if(coplista[i].IsBusy())
                {
                    if(!coplista[i].getSuspect().Exists())
                    {
                        coplista[i].getPed().CurrentVehicle.SirenActive = false;
                        coplista[i].getPed().Task.ClearAll();
                        coplista[i].getSuspect().Task.ClearAll();
                        coplista[i].SetBusy(false);
                    }
                    if(Vector3.Distance(coplista[i].getPed().Position, coplista[i].getSuspect().Position) < 7 && coplista[i].getPed().CurrentVehicle.Speed < 2)
                    {
                        Function.Call(Hash.DRAW_LINE, coplista[i].getPed().Position.X, coplista[i].getPed().Position.Y, coplista[i].getPed().Position.Z,
                            coplista[i].getSuspect().Position.X, coplista[i].getSuspect().Position.Y, coplista[i].getSuspect().Position.Z, 0, 255, 0, 255);

                        coplista[i].getPed().CurrentVehicle.SirenActive = false;
                        coplista[i].getPed().Task.ClearAll();
                        coplista[i].getSuspect().Task.ClearAll();
                        coplista[i].SetBusy(false);
                    }
                }
            }
        }

        void ListClearer()
        {
            for (int i = 0; i < pedlista.Count; i++)
            {
                if (!pedlista[i].getPed().Exists())
                    pedlista.Remove(pedlista[i]);
            }
            for (int i = 0; i < coplista.Count; i++)
            {
                if (!coplista[i].getPed().Exists())
                    coplista.Remove(coplista[i]);
            }
        }
    }

    class PedHandler
    {
        Ped ped;
        public PedHandler(Ped ped)
        {
            this.ped = ped;
        }

        public bool Violating()
        {
            if(ped.IsInVehicle())
            {
                if (ped.CurrentVehicle.Speed > 15)
                    return true;
            }
            return false;
        }

        public Ped getPed()
        {
            return ped;
        }
    }

    class CopHandler
    {
        Ped ped;
        Ped suspect;
        bool isbusy = false;

        public CopHandler(Ped ped)
        {
            this.ped = ped;
        }

        public bool IsBusy()
        {
            if (isbusy)
                return true;
            return false;
        }

        public Ped getPed()
        {
            return ped;
        }

        public void SetBusy(bool value)
        {
            isbusy = value;
        }

        public void SetSuspect(Ped suspect)
        {
            this.suspect = suspect;
        }

        public Ped getSuspect()
        {
            return suspect;
        }
    }
}
